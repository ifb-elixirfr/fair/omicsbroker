#!/bin/sh

docker-compose exec web python manage.py migrate
docker-compose exec web python manage.py load_countries
docker-compose exec web python manage.py load_app_users
docker-compose exec web python manage.py load_projects
docker-compose exec web python manage.py load_work_on
docker-compose exec web python manage.py import_faq
docker-compose exec web python manage.py load_GISAID
docker-compose exec web python manage.py load_ENA