from django.utils import timezone

from django.db import models

from apps.users.models import applicationUser


class tag(models.Model):
    label = models.CharField(help_text="Tag label", max_length=50)
    color = models.CharField(help_text="Tag color", max_length=8,
                             default="#e66465")


class project(models.Model):
    ACCESS = (
        ('Public', 'Public'),
        ('Private', 'Private'),
    )

    applicationUsers = models.ManyToManyField(applicationUser,
                                              through='work_on')
    tags = models.ManyToManyField(tag,
                                  through='tag_project')
    title = models.CharField(max_length=500, help_text="Project Title", )
    alias = models.CharField(max_length=100, help_text="Project alias", )
    description = models.TextField(help_text="Project description", )
    notepad = models.JSONField(help_text="Project notepad",
                               blank=True,
                               null=True,
                               )
    created_date = models.DateTimeField(default=timezone.now)
    last_modification_date = models.DateTimeField(default=timezone.now)
    access = models.CharField(max_length=7, choices=ACCESS, default='Private',
                              help_text="Metadata access: public (all users of"
                                        " the omicsBroker instance) or private"
                                        " (only users associated with the "
                                        "project). Default: private", )

    def __str__(self):
        return self.title


class work_on(models.Model):
    PERMISSION = (
        ('All', 'Read, Edit, Manage'),
        ('Edit', 'Read, Edit'),
        ('Read', 'Read'),
    )
    contact = models.BooleanField(help_text="Project contact",
                                  default=False)

    permission = models.CharField(max_length=4, choices=PERMISSION,
                                  default='Read',
                                  help_text="Management permission : all "
                                            "(read, edit and delete), Edit "
                                            "(read and edit) and Read "
                                            "(read only). By default : Read", )

    applicationUser = models.ForeignKey(applicationUser,
                                        on_delete=models.CASCADE)
    project = models.ForeignKey(project, on_delete=models.CASCADE)


class tag_project(models.Model):
    tag = models.ForeignKey(tag,
                            on_delete=models.CASCADE)
    project = models.ForeignKey(project, on_delete=models.CASCADE)
