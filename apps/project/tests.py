import json
from http import HTTPStatus

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from apps.countries.models import countries
from apps.metadata.models import database
from apps.project.admin import work_onAdmin
from apps.project.models import project, work_on, tag, tag_project
from apps.users.models import applicationUser


class projectTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        p = project.objects.create(
            title="Great project",
            alias="great_project",
            description='a description')

        database.objects.create(
            name="toto"
        )

        u = User.objects.create_user(
            username='userProject',
            email='userProject@mail.com',
            password='djangoPWD')

        u2 = User.objects.create_user(
            username='userProject2',
            email='userProject2@mail.com',
            password='djangoPWD')

        u3 = User.objects.create_user(
            username='userProject3',
            email='userProject3@mail.com',
            password='djangoPWD')

        c = countries.objects.create(
            ADMIN="Aruba", ISO_A3="ABW",
            geometry='{ "type": "Feature", \
                    "properties": { "ADMIN": "Aruba", "ISO_A3": "ABW" }, \
                    "geometry": { "type": "Polygon",\
                    "coordinates": [ [ \
                    [ -69.996937628999916, 12.577582098000036 ], \
                    [ -69.936390753999945, 12.531724351000051 ],\
                    [ -69.924672003999945, 12.519232489000046 ],\
                    [ -69.915760870999918, 12.497015692000076 ],\
                    [ -69.880197719999842, 12.453558661000045 ],\
                    [ -69.876820441999939, 12.427394924000097 ],\
                    [ -69.888091600999928, 12.417669989000046 ],\
                    [ -69.908802863999938, 12.417792059000107 ],\
                    [ -69.930531378999888, 12.425970770000035 ],\
                    [ -69.945139126999919, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.447211005000014 ],\
                    [ -69.958566860999923, 12.463202216000099 ],\
                    [ -70.027658657999922, 12.522935289000088 ],\
                    [ -70.048085089999887, 12.531154690000079 ],\
                    [ -70.058094855999883, 12.537176825000088 ],\
                    [ -70.062408006999874, 12.546820380000057 ],\
                    [ -70.060373501999948, 12.556952216000113 ],\
                    [ -70.051096157999893, 12.574042059000064 ],\
                    [ -70.048736131999931, 12.583726304000024 ],\
                    [ -70.052642381999931, 12.600002346000053 ],\
                    [ -70.059641079999921, 12.614243882000054 ],\
                    [ -70.061105923999975, 12.625392971000068 ],\
                    [ -70.048736131999931, 12.632147528000104 ],\
                    [ -70.00715084499987, 12.5855166690001 ],\
                    [ -69.996937628999916, 12.577582098000036 ] ] ] } }')

        user_temp = applicationUser.objects.create(
            user=u,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        user_temp.save()

        user_temp2 = applicationUser.objects.create(
            user=u2,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        applicationUser.objects.create(
            user=u3,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        work_on.objects.create(
            applicationUser=user_temp,
            project=p,
            permission='All'
        )

        work_on.objects.create(
            applicationUser=user_temp2,
            project=p,
            permission='All'
        )

        t = tag.objects.create(
            label="test")

        tag_project.objects.create(
            tag=t,
            project=p)

    def test_str_project(self):
        project_test = project.objects.all()[0]
        self.assertEquals(str(project_test), project_test.title)

    def test_project_list_in_view(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        url = reverse('project_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_project_list_out_view(self):
        url = reverse('project_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    ###########################################################################
    # Project detail
    ###########################################################################
    def test_project_detail_in(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        project_test = project.objects.all()[0]
        self.assertTrue(logged_in)
        url = reverse('project_detail', kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_project_detail_in_wrong_user(self):
        logged_in = self.client.login(username="userProject3",
                                      password="djangoPWD")
        project_test = project.objects.all()[0]
        self.assertTrue(logged_in)
        url = reverse('project_detail', kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_project_detail_out(self):
        project_test = project.objects.all()[0]
        url = reverse('project_detail', kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_project_detail_user_not_in_project(self):
        logged_in = self.client.login(username="userProject2",
                                      password="djangoPWD")
        project_test = project.objects.all()[0]
        self.assertTrue(logged_in)
        url = reverse('project_detail',
                      kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_project_create_work_on(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_detail',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={"txtSearch": "userProject3",
                                          "permission": "Edit",
                                          "create_work_on": "create_work_on"})
        testCreate = work_on.objects.filter(
            applicationUser__user__username="userProject3",
            project__id=project_test.id).exists()

        self.assertTrue(testCreate)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_modify_work_on(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_detail',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={"username_input": "userProject2",
                                          "permission": "Read",
                                          "permission_btn": "permission_btn"})
        testCreate = work_on.objects.filter(
            applicationUser__user__username="userProject2",
            project__id=project_test.id,
            permission="Read").exists()
        self.assertTrue(testCreate)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_modify_own_work_on(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_detail',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={"username_input": "userProject",
                                          "permission": "Read",
                                          "permission_btn": "permission_btn"})
        testCreate = work_on.objects.filter(
            applicationUser__user__username="userProject",
            project__id=project_test.id,
            permission="All").exists()
        self.assertTrue(testCreate)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_delete_work_on(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_delete_collaborator',
                      kwargs={'id_project': project_test.id,
                              'username': 'userProject2'})
        response = self.client.get(url)

        testExist = work_on.objects.filter(
            applicationUser__user__username="userProject2",
            project__id=project_test.id).exists()
        self.assertFalse(testExist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_project_delete_work_on_himself(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_delete_collaborator',
                      kwargs={'id_project': project_test.id,
                              'username': 'userProject'})
        response = self.client.get(url)

        testExist = work_on.objects.filter(
            applicationUser__user__username="userProject",
            project__id=project_test.id).exists()
        self.assertTrue(testExist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_project_delete_work_on_wrong_user(self):
        logged_in = self.client.login(username="userProject3",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_delete_collaborator',
                      kwargs={'id_project': project_test.id,
                              'username': 'userProject'})
        response = self.client.get(url)

        testExist = work_on.objects.filter(
            applicationUser__user__username="userProject",
            project__id=project_test.id).exists()
        self.assertTrue(testExist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_project_delete_work_on_logout(self):
        project_test = project.objects.all()[0]
        url = reverse('project_delete_collaborator',
                      kwargs={'id_project': project_test.id,
                              'username': 'userProject'})
        response = self.client.get(url)
        testExist = work_on.objects.filter(
            applicationUser__user__username="userProject",
            project__id=project_test.id).exists()
        self.assertTrue(testExist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    ###########################################################################
    # Project detail - TAG
    ###########################################################################
    def test_project_create_tag(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_detail',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={"tag_label": "test2",
                                          "tag_color": "#aec7ea",
                                          "create_tag_project":
                                              "create_tag_project"})
        test_create = tag_project.objects.filter(
            tag__label="test2",
            project__id=project_test.id).exists()

        self.assertTrue(test_create)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_create_tag_existing(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_detail',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={"tag_label": "test",
                                          "tag_color": "#aec7ea",
                                          "create_tag_project":
                                              "create_tag_project"})
        test_create = tag_project.objects.filter(
            tag__label="test",
            project__id=project_test.id).exists()

        self.assertTrue(test_create)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_delete_tag_project(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_detail',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={"select_remove_tag": "test",
                                          "remove_tag_project":
                                              "remove_tag_project"})

        test_exist = tag_project.objects.filter(
            tag__label="test",
            project__id=project_test.id).exists()
        self.assertFalse(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    ###########################################################################
    # Project creation
    ###########################################################################
    def test_project_create_in(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        url = reverse('project_create')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_project_create_out(self):
        url = reverse('project_create')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_project_create(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        url = reverse('project_create')
        response = self.client.post(url,
                                    data={"title": "Project",
                                          "alias": "Alias project",
                                          "description": "A description"})
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    ###########################################################################
    # Project edit
    ###########################################################################
    def test_project_edit_in(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_edit',
                      kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_edit_out(self):
        project_test = project.objects.all()[0]
        url = reverse('project_edit',
                      kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_edit_in_post(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_edit',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={"title": "Project",
                                          "alias": "Alias project",
                                          "description": "A description"})
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    ###########################################################################
    # Project admin
    ###########################################################################
    def test_admin_username(self):
        wo = work_on.objects.all()[0]
        admin_function_result = work_onAdmin.get_username(self, wo)
        self.assertEquals(admin_function_result,
                          wo.applicationUser.user.username)

    def test_admin_title(self):
        wo = work_on.objects.all()[0]
        admin_function_result = work_onAdmin.get_title(self, wo)
        self.assertEquals(admin_function_result,
                          wo.project.title)

    ###########################################################################
    # Project delete
    ###########################################################################
    def test_project_delete_in(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_delete',
                      kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_project_delete_in_wrong_user(self):
        logged_in = self.client.login(username="userProject3",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('project_delete',
                      kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_project_delete_out(self):
        project_test = project.objects.all()[0]
        url = reverse('project_delete',
                      kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    ###########################################################################
    # AJAX
    ###########################################################################
    def test_project_ajax(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        url = reverse('autocomplete_username')
        response = self.client.post(url, {"foo": "bar"},
                                    **{'HTTP_X_REQUESTED_WITH':
                                    'XMLHttpRequest'})
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_ajax_fail(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        url = reverse('autocomplete_username')
        response = self.client.post(url, {"foo": "bar"})
        self.assertEqual(response.content, b"fail")
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_project_ajax_save(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('save_notepad',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    json.dumps({'json': 'object'}),
                                    content_type="application/json")
        self.assertEqual(response.status_code, HTTPStatus.OK)
