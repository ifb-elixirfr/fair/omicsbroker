import csv

from django.core.management.base import BaseCommand

from apps.project.models import project


class Command(BaseCommand):
    help = 'Reload regions data'

    def handle(self, *args, **kwargs):
        with open('apps/project/static/data/project.csv',
                  newline='', encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
            next(spamreader, None)
            for row in spamreader:
                project.objects.create(
                    title=row[0],
                    alias=row[1],
                    description=row[2])

        self.stdout.write("Done")
