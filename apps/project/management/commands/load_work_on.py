import csv

from django.core.management.base import BaseCommand

from apps.project.models import project, work_on
from apps.users.models import applicationUser


class Command(BaseCommand):
    help = 'Reload regions data'

    def handle(self, *args, **kwargs):
        with open('apps/project/static/data/work_on.csv',
                  newline='', encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
            next(spamreader, None)
            for row in spamreader:
                project_temp = project.objects.get(title=row[0])
                user_temp = applicationUser.objects.get(user__username=row[1])
                work_on.objects.create(
                    applicationUser=user_temp,
                    project=project_temp,
                    permission='All'
                )

        self.stdout.write("Done")
