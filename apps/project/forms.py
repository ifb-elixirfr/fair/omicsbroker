from django import forms
from .models import project


class ProjectCreateForm(forms.ModelForm):
    class Meta:
        model = project
        fields = ('title', 'alias', 'description')
