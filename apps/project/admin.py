from django.contrib import admin

from apps.project.models import project, work_on, tag, tag_project
from apps.home.admin import admin_site


@admin.register(project, site=admin_site)
class projectAdmin(admin.ModelAdmin):
    save_on_top = True


@admin.register(work_on, site=admin_site)
class work_onAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('get_username', 'get_title', 'permission')

    def get_username(self, obj):
        return work_on.objects.get(id=obj.pk).applicationUser.user.username

    def get_title(self, obj):
        return work_on.objects.get(id=obj.pk).project.title

    get_username.short_description = 'Username'
    get_title.short_description = 'Title'


@admin.register(tag_project, site=admin_site)
class tag_projectAdmin(admin.ModelAdmin):
    save_on_top = True


@admin.register(tag, site=admin_site)
class tagAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('label', 'color')
