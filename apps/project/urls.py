from django.urls import path

from apps.project.views import project_list, project_edit, project_create, \
    project_detail, project_delete, autocomplete_username, \
    project_delete_collaborator, save_notepad

urlpatterns = [
    path('project/<int:id_project>', project_detail, name='project_detail'),
    path('project/<int:id_project>/<str:message_action>',
         project_detail, name='project_detail_message'),
    path('project/<int:id_project>/delete/collaborator/<str:username>',
         project_delete_collaborator, name='project_delete_collaborator'),
    path('project/<int:id_project>/edit/', project_edit, name='project_edit'),
    path('project/create/', project_create, name='project_create'),
    path('project/<int:id_project>/delete/', project_delete,
         name='project_delete'),

    path("my_projects/", project_list, name="project_list"),
    path("my_projects/<str:message_action>", project_list,
         name="project_list_message"),

    path('ajax_calls/search/', autocomplete_username,
         name="autocomplete_username"),
    path('ajax_calls/save/<int:id_project>', save_notepad,
         name="save_notepad"),

]
