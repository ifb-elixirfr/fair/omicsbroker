import json

from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.utils import timezone
from django.shortcuts import render, get_object_or_404, redirect

from apps.metadata.models import database, metadata
from apps.project.forms import ProjectCreateForm
from apps.project.models import project, work_on, tag_project, tag
from apps.users.models import applicationUser


def project_list(request, message_action=None):
    if request.user.is_authenticated:
        user_temp = applicationUser.objects.get(
            user__username=request.user.username)
        list_projects = user_temp.project_set.all()
        return render(request, 'project/user_projects.html', context={
            'list_projects': list_projects,
            'message': message_action
        })
    else:
        return render(request, 'project/user_projects.html', context={
            'message': "If you wish to see your projects, you must log in.",
        })


def project_detail(request, id_project, message_action=None):
    if request.user.is_authenticated:
        project_element = get_object_or_404(project, id=id_project)

        r = work_on.objects.filter(
            applicationUser__user__username=request.user.username,
            project__id=id_project)
        if r.exists():
            if request.method == "POST":

                if 'remove_tag_project' in request.POST:
                    test = tag_project.objects.filter(
                        tag__label=request.POST["select_remove_tag"],
                        project__id=id_project)

                    if test.exists():
                        tag_project.objects.get(
                            tag__label=request.POST[
                                "select_remove_tag"]).delete()

                if 'create_tag_project' in request.POST:
                    t = tag.objects.filter(
                        label=request.POST["tag_label"])

                    if not t.exists():
                        t = tag.objects.create(
                            label=request.POST["tag_label"],
                            color=request.POST["tag_color"])

                    test = tag_project.objects.filter(
                        tag__label=request.POST["tag_label"],
                        project__id=id_project)

                    if not test.exists():
                        t = tag.objects.get(label=request.POST["tag_label"])
                        p = project.objects.get(id=id_project)
                        tag_project.objects.create(
                            tag=t,
                            project=p
                        )

                if 'create_work_on' in request.POST:
                    test = work_on.objects.filter(
                        applicationUser__user__username=request.POST[
                            "txtSearch"],
                        project__id=id_project)

                    if not test.exists():
                        a = applicationUser.objects.get(
                            user__username=request.POST["txtSearch"])
                        p = project.objects.get(id=id_project)

                        work_on.objects.create(
                            applicationUser=a,
                            project=p,
                            permission=request.POST["permission"]
                        )
                elif 'permission_btn' in request.POST:
                    if request.user.username != request.POST[
                                                            "username_input"]:
                        wo = work_on.objects.get(
                            applicationUser__user__username=request.POST[
                                "username_input"],
                            project__id=id_project)
                        wo.permission = request.POST["permission"]
                        wo.save()
                    else:
                        message_action = "You can't change your " \
                                         "own permission."

            collaborators = work_on.objects.filter(project__id=id_project)
            tags = tag_project.objects.filter(project__id=id_project)
            meta = metadata.objects.filter(project__id=id_project)
            db = database.objects.all()
            return render(request, 'project/project_detail.html',
                          {'project': project_element,
                           "collaborators": collaborators,
                           "user_permission": r.values_list('permission',
                                                            flat=True)[0],
                           "tags": tags,
                           "meta": meta,
                           "db": db,
                           "permission": work_on.PERMISSION,
                           "message_action": message_action})
        else:
            return render(request, 'project/project_detail.html', context={
                'message': "It's not your project !",
            })
    else:
        return render(request, 'project/project_detail.html', context={
            'message': "If you wish to modify a project, "
                       "you must be identified.",
        })


def project_edit(request, id_project):
    if request.user.is_authenticated:
        project_element = get_object_or_404(project, id=id_project)
        if request.method == "POST":
            form = ProjectCreateForm(request.POST, instance=project_element)
            if form.is_valid():
                inter = form.save(commit=False)
                inter = inter
                inter.last_modification_date = timezone.now()
                inter.save()
                return redirect('project_detail',
                                id_project=project_element.id)
        else:
            form = ProjectCreateForm(instance=project_element)
        return render(request, 'project/project_edit.html',
                      {'id_project': id_project,
                       'form': form})
    else:
        return render(request, 'project/project_edit.html',
                      {'id_project': id_project,
                       'message': "If you wish to create a project, "
                                  "you must be identified."})


def project_create(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = ProjectCreateForm(request.POST)
            if form.is_valid():
                p = form.save()
                ap = applicationUser.objects.get(user=request.user)
                wo = work_on.objects.create(contact=True,
                                            permission='All',
                                            project=p,
                                            applicationUser=ap)
                wo.save()

                return redirect('project_detail', id_project=p.id)
        else:
            form = ProjectCreateForm()
        return render(request, 'project/project_create.html', {'form': form})
    else:
        return render(request, 'project/project_create.html',
                      {'message': "If you wish to create a project, "
                                  "you must be identified."})


def project_delete(request, id_project):
    if request.user.is_authenticated:
        project_element = get_object_or_404(project, id=id_project)

        r = work_on.objects.filter(
            applicationUser__user__username=request.user.username,
            project__id=id_project)

        if r.exists():
            project_element.delete()
            return HttpResponseRedirect(
                reverse("project_list_message",
                        args=[
                            "The project are deleted"]))
        else:
            return HttpResponseRedirect(
                reverse("project_list_message",
                        args=[
                            "It's not your project !"]))
    else:
        return HttpResponseRedirect(
            reverse("project_list_message",
                    args=[
                        "If you wish to delete projects, you must log in."]))


def project_delete_collaborator(request, id_project, username):
    if request.user.is_authenticated:
        project_element = get_object_or_404(project, id=id_project)

        r = work_on.objects.filter(
            applicationUser__user__username=request.user.username,
            project__id=id_project, permission='All')

        if r.exists() and project_element:
            if request.user.username == username:
                return HttpResponseRedirect(
                    reverse("project_detail_message",
                            args=[
                                id_project,
                                "You can't delete yourself !"]))
            else:
                work_on.objects.get(project__id=id_project,
                                    applicationUser__user__username=username) \
                    .delete()
                return HttpResponseRedirect(
                    reverse("project_detail_message",
                            args=[id_project,
                                  'The collaborator has been withdrawn '
                                  'from the project']))
        else:
            return HttpResponseRedirect(
                reverse("project_detail_message",
                        args=[id_project,
                              "You can't do that !"]))
    else:
        return HttpResponseRedirect(
            reverse("project_detail_message",
                    args=[id_project,
                          "If you wish to delete projects, you must log in."]))


def autocomplete_username(request):
    if request.is_ajax():
        q = request.GET.get('query', '')
        result = list(applicationUser.objects.filter(
            user__username__icontains=q).values_list('user__username',
                                                     flat=True))
        data = json.dumps({"query": "query", "suggestions": result},
                          cls=DjangoJSONEncoder)

    else:
        data = 'fail'

    return HttpResponse(data, content_type='application/json;charset=utf-8')


def save_notepad(request, id_project):
    project_element = get_object_or_404(project, id=id_project)
    project_element.notepad = json.loads(request.body)
    project_element.save()
    return HttpResponse('Ok')
