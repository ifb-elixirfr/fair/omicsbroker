import csv
import json

from django.core.management.base import BaseCommand

from apps.faq.models import category, question


class Command(BaseCommand):
    help = 'Import FAQ'

    def handle(self, *args, **kwargs):
        with open('apps/faq/static/data/category.tsv',
                  newline='', encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
            next(spamreader, None)
            for row in spamreader:
                category.objects.create(
                    name=row[0],
                    position=row[1]
                )

        self.stdout.write("Categories : Imported")

        with open('apps/faq/static/data/question.tsv',
                  newline='', encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
            next(spamreader, None)
            for row in spamreader:
                cat = category.objects.get(name=row[2])
                question.objects.create(
                    question=row[0],
                    response=json.loads(row[1]),
                    category=cat,
                    position=row[3]
                )

        self.stdout.write("Questions : Imported")
