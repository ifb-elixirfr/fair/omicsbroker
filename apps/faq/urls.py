from django.urls import path

from apps.faq.views import faq_list, question_confirm_delete, \
    question_delete, question_add, question_edit, cat_modify, cat_delete, \
    cat_confirm_delete, cat_remove_unused_confirm, cat_remove_unused

urlpatterns = [
    path('FAQ/', faq_list, name='faq_list'),
    path('FAQ/<str:message>', faq_list, name='faq_list'),

    path('question/add/', question_add,
         name='question_add'),
    path('question/<int:id_question>/confirm/delete/', question_confirm_delete,
         name='question_confirm_delete'),
    path('question/<int:id_question>/delete/', question_delete,
         name='question_delete'),
    path('question/<int:id_question>/edit/', question_edit,
         name='question_edit'),
    path('category/<int:id_category>/modify/', cat_modify,
         name='cat_modify'),

    path('category/<int:id_category>/confirm/delete', cat_confirm_delete,
         name='cat_confirm_delete'),
    path('category/<int:id_category>/delete', cat_delete,
         name='cat_delete'),

    path('category/unsed/confirm/delete', cat_remove_unused_confirm,
         name='cat_remove_unused_confirm'),

    path('category/unsed/delete', cat_remove_unused,
         name='cat_remove_unused'),

]
