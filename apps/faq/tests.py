from http import HTTPStatus

from django.contrib.auth.models import User
from django.db.models import Max
from django.test import TestCase
from django.urls import reverse

from apps.countries.models import countries
from apps.faq.models import category, question
from apps.users.models import applicationUser


class FAQTestCase(TestCase):
    def setUp(self):
        u = User.objects.create_superuser('userStaff',
                                          'userStaff@mail.com',
                                          "superPDW")

        u2 = User.objects.create_user(
            username='userNormal',
            email='userProject2@mail.com',
            password='djangoPWD')

        c = countries.objects.create(
            ADMIN="Aruba", ISO_A3="ABW",
            geometry='{ "type": "Feature", \
                    "properties": { "ADMIN": "Aruba", "ISO_A3": "ABW" }, \
                    "geometry": { "type": "Polygon",\
                    "coordinates": [ [ \
                    [ -69.996937628999916, 12.577582098000036 ], \
                    [ -69.936390753999945, 12.531724351000051 ],\
                    [ -69.924672003999945, 12.519232489000046 ],\
                    [ -69.915760870999918, 12.497015692000076 ],\
                    [ -69.880197719999842, 12.453558661000045 ],\
                    [ -69.876820441999939, 12.427394924000097 ],\
                    [ -69.888091600999928, 12.417669989000046 ],\
                    [ -69.908802863999938, 12.417792059000107 ],\
                    [ -69.930531378999888, 12.425970770000035 ],\
                    [ -69.945139126999919, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.447211005000014 ],\
                    [ -69.958566860999923, 12.463202216000099 ],\
                    [ -70.027658657999922, 12.522935289000088 ],\
                    [ -70.048085089999887, 12.531154690000079 ],\
                    [ -70.058094855999883, 12.537176825000088 ],\
                    [ -70.062408006999874, 12.546820380000057 ],\
                    [ -70.060373501999948, 12.556952216000113 ],\
                    [ -70.051096157999893, 12.574042059000064 ],\
                    [ -70.048736131999931, 12.583726304000024 ],\
                    [ -70.052642381999931, 12.600002346000053 ],\
                    [ -70.059641079999921, 12.614243882000054 ],\
                    [ -70.061105923999975, 12.625392971000068 ],\
                    [ -70.048736131999931, 12.632147528000104 ],\
                    [ -70.00715084499987, 12.5855166690001 ],\
                    [ -69.996937628999916, 12.577582098000036 ] ] ] } }')

        applicationUser.objects.create(
            user=u,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        applicationUser.objects.create(
            user=u2,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        c = category.objects.create(
            name="test",
            position=1
        )

        c2 = category.objects.create(
            name="Delete cat",
            position=2
        )

        category.objects.create(
            name="Unused",
            position=3
        )

        question.objects.create(
            question="Why ?",
            response="Because",
            category=c,
            position=1
        )

        question.objects.create(
            question="How ?",
            response="Because",
            category=c,
            position=1
        )

        question.objects.create(
            question="Who ?",
            response="Me",
            category=c2,
            position=1
        )

    def test_category_str(self):
        cat = category.objects.get(name="test")
        self.assertEqual(str(cat), cat.name + " " + str(cat.position))

    def test_question_str(self):
        ques = question.objects.get(question="Why ?")
        self.assertEqual(str(ques), ques.question)

    def test_faq_load_page(self):
        url = reverse('faq_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_faq_load_page_message(self):
        url = reverse('faq_list', kwargs={'message': 'Hello !'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    ###########################################################################
    # Category
    ###########################################################################

    # =========================================================================
    # Confirm delete category
    # =========================================================================

    def test_category_confirm_delete_login_staff(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        cat = category.objects.get(name="Delete cat")
        url = reverse('cat_confirm_delete',
                      kwargs={'id_category': cat.id})
        response = self.client.get(url)
        self.assertTemplateUsed(response,
                                'faq/cat_confirm_delete.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_category_confirm_delete_login_staff_no_exist(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        random_id = question.objects.aggregate(Max('id'))['id__max'] + 1
        url = reverse('cat_confirm_delete',
                      kwargs={'id_category': random_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_category_confirm_delete_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        cat = category.objects.get(name="Delete cat")
        url = reverse('cat_confirm_delete',
                      kwargs={'id_category': cat.id})
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_category_confirm_delete_logout(self):
        cat = category.objects.get(name="Delete cat")
        url = reverse('cat_confirm_delete',
                      kwargs={'id_category': cat.id})
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    # =========================================================================
    # Delete category
    # =========================================================================

    def test_category_delete_login_staff(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        cat = category.objects.get(name="Delete cat")
        url = reverse('cat_delete',
                      kwargs={'id_category': cat.id})
        response = self.client.get(url)

        test_exist = category.objects.filter(name="Delete cat").exists()
        self.assertFalse(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_category_delete_login_staff_no_question(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)
        random_id = category.objects.aggregate(Max('id'))['id__max'] + 1
        url = reverse('cat_delete',
                      kwargs={'id_category': random_id})
        response = self.client.get(url)

        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_category_delete_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        cat = category.objects.get(name="Delete cat")
        url = reverse('cat_delete',
                      kwargs={'id_category': cat.id})
        response = self.client.get(url)

        test_exist = category.objects.filter(name="Delete cat").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_category_delete_logout(self):
        cat = category.objects.get(name="Delete cat")
        url = reverse('cat_delete',
                      kwargs={'id_category': cat.id})
        response = self.client.get(url)

        test_exist = category.objects.filter(name="Delete cat").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    # =========================================================================
    # Confirm delete unused categories
    # =========================================================================

    def test_unused_category_confirm_delete_login_staff(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        url = reverse('cat_remove_unused_confirm')
        response = self.client.get(url)
        self.assertTemplateUsed(response,
                                'faq/cat_confirm_delete_unused.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_unused_category_confirm_delete_login_staff_no_exist(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        url = reverse('cat_remove_unused')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        url = reverse('cat_remove_unused_confirm')
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_unused_category_confirm_delete_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        url = reverse('cat_remove_unused_confirm')
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_unused_category_confirm_delete_logout(self):
        url = reverse('cat_remove_unused_confirm')
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    # =========================================================================
    # Delete unused categories
    # =========================================================================

    def test_unused_category_delete_login_staff(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        url = reverse('cat_remove_unused')
        response = self.client.get(url)

        test_exist = category.objects.filter(name="Unused").exists()
        self.assertFalse(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_unused_category_delete_login_staff_no_question(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)
        url = reverse('cat_remove_unused')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        url = reverse('cat_remove_unused')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_unused_category_delete_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        url = reverse('cat_remove_unused')
        response = self.client.get(url)

        test_exist = category.objects.filter(name="Unused").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_unused_category_delete_logout(self):
        url = reverse('cat_remove_unused')
        response = self.client.get(url)

        test_exist = category.objects.filter(name="Unused").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    # =========================================================================
    # Modify category
    # =========================================================================

    def test_category_modify_login_staff(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        cat = category.objects.all()[0]
        url = reverse('cat_modify',
                      kwargs={'id_category': cat.id})
        response = self.client.post(url,
                                    data={"new_cat_name": "new name",
                                          "new_cat_position": 2})

        testCreate = category.objects.filter(name="new name",
                                             position=2).exists()
        self.assertTrue(testCreate)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_category_modify_login_staff_get(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        cat = category.objects.all()[0]
        url = reverse('cat_modify',
                      kwargs={'id_category': cat.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_category_modify_login_no_staff(self):
        logged_in = self.client.login(username='userNormal',
                                      password='djangoPWD')
        self.assertTrue(logged_in)

        cat = category.objects.all()[0]
        url = reverse('cat_modify',
                      kwargs={'id_category': cat.id})
        response = self.client.post(url,
                                    data={"new_cat_name": "new name",
                                          "new_cat_position": 2})

        testCreate = category.objects.filter(name="test",
                                             position=1).exists()
        self.assertTrue(testCreate)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_category_modify_logout(self):
        cat = category.objects.all()[0]
        url = reverse('cat_modify',
                      kwargs={'id_category': cat.id})
        response = self.client.post(url,
                                    data={"new_cat_name": "new name",
                                          "new_cat_position": 2})

        testCreate = category.objects.filter(name="test",
                                             position=1).exists()
        self.assertTrue(testCreate)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    ###########################################################################
    # Question
    ###########################################################################

    # =========================================================================
    # Confirm delete question
    # =========================================================================

    def test_question_confirm_delete_login_staff(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        ques = question.objects.get(question="How ?")
        url = reverse('question_confirm_delete',
                      kwargs={'id_question': ques.id})
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/question_confirm_delete.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_confirm_delete_login_staff_no_exist(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        random_id = question.objects.aggregate(Max('id'))['id__max'] + 1
        url = reverse('question_confirm_delete',
                      kwargs={'id_question': random_id})
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_confirm_delete_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        ques = question.objects.get(question="How ?")
        url = reverse('question_confirm_delete',
                      kwargs={'id_question': ques.id})
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_confirm_delete_logout(self):
        ques = question.objects.get(question="How ?")
        url = reverse('question_confirm_delete',
                      kwargs={'id_question': ques.id})
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    # =========================================================================
    # Delete question
    # =========================================================================

    def test_question_delete_login_staff(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        ques = question.objects.get(question="How ?")
        url = reverse('question_delete',
                      kwargs={'id_question': ques.id})
        response = self.client.get(url)

        test_exist = question.objects.filter(question="How ?").exists()
        self.assertFalse(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_question_delete_login_staff_no_question(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)
        random_id = question.objects.aggregate(Max('id'))['id__max'] + 1
        url = reverse('question_delete',
                      kwargs={'id_question': random_id})
        response = self.client.get(url)

        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_delete_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        ques = question.objects.get(question="How ?")
        url = reverse('question_delete',
                      kwargs={'id_question': ques.id})
        response = self.client.get(url)

        test_exist = question.objects.filter(question="How ?").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_delete_logout(self):
        ques = question.objects.get(question="How ?")
        url = reverse('question_delete',
                      kwargs={'id_question': ques.id})
        response = self.client.get(url)

        test_exist = question.objects.filter(question="How ?").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    # =========================================================================
    # Create question
    # =========================================================================

    def test_question_create_logout(self):
        url = reverse('question_add')
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_create_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        url = reverse('question_add')
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_create_login_staff(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        url = reverse('question_add')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_create_login_staff_post_cat(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        url = reverse('question_add')
        response = self.client.post(url,
                                    data={"cat_name": "toto",
                                          "cat_position": 1,
                                          "create_cat": "create_cat"})

        test_exist = category.objects.filter(name="toto").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_create_login_staff_post_question(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        url = reverse('question_add')
        response = self.client.post(url,
                                    data={"select_cat": "test",
                                          "question": "question ?",
                                          "question_position": 1,
                                          "editorjs_content": '{"foo": "bar"}',
                                          "create_question":
                                              "create_question"})

        test_exist = question.objects.filter(question="question ?").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    # =========================================================================
    # Edit question
    # =========================================================================

    def test_question_edit_logout(self):
        ques = question.objects.get(question="How ?")
        url = reverse('question_edit',
                      kwargs={'id_question': ques.id}
                      )
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_edit_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        ques = question.objects.get(question="How ?")
        url = reverse('question_edit',
                      kwargs={'id_question': ques.id}
                      )
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'faq/faq.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_edit_login_staff_get(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        ques = question.objects.get(question="How ?")
        url = reverse('question_edit',
                      kwargs={'id_question': ques.id}
                      )
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_edit_login_staff_get_no_exist(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        random_id = question.objects.aggregate(Max('id'))['id__max'] + 1
        url = reverse('question_edit',
                      kwargs={'id_question': random_id}
                      )
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_edit_login_staff_post_cat(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        ques = question.objects.get(question="How ?")
        url = reverse('question_edit',
                      kwargs={'id_question': ques.id}
                      )
        response = self.client.post(url,
                                    data={"cat_name": "toto",
                                          "cat_position": 1,
                                          "create_cat": "create_cat"})

        test_exist = category.objects.filter(name="toto").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_question_edit_login_staff_post_question(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        ques = question.objects.get(question="How ?")
        url = reverse('question_edit',
                      kwargs={'id_question': ques.id}
                      )
        response = self.client.post(url,
                                    data={"select_cat": "test",
                                          "question": "question ?",
                                          "question_position": 1,
                                          "editorjs_content": '{"foo": "bar"}',
                                          "edit_question":
                                              "edit_question"})

        test_exist = question.objects.filter(
            question="question ?").exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
