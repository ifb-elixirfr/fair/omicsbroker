from django.contrib import admin

from apps.faq.models import category, question
from apps.home.admin import admin_site


@admin.register(category, site=admin_site)
class categoryAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('name', 'position')
    ordering = ('position',)

    class Meta:
        verbose_name_plural = "Categories"
        ordering = ['position']


@admin.register(question, site=admin_site)
class questionAdmin(admin.ModelAdmin):
    save_on_top = True
