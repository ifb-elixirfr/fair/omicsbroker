import json

from django.shortcuts import render, redirect
from django.urls import reverse

from apps.faq.models import question, category


def faq_list(request, message=None):
    questions = question.objects.order_by('category__position',
                                          'position')
    if message:
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_delete': message})
    else:
        return render(request, 'faq/faq.html',
                      {'questions': questions})


###############################################################################
# Manage Question
###############################################################################

def question_edit(request, id_question):
    if request.user.is_authenticated and request.user.is_staff:
        question_element = question.objects.filter(id=id_question)

        if not question_element.exists():
            questions = question.objects.order_by('category__position',
                                                  'position')
            return render(request, 'faq/faq.html',
                          {'questions': questions})
        else:
            categories = category.objects.all()
            question_element = question.objects.get(id=id_question)

            if request.method == "POST":
                if 'create_cat' in request.POST:
                    cat = category.objects.filter(
                        name__iexact=request.POST["cat_name"])
                    if not cat.exists():
                        category.objects.create(
                            name=request.POST["cat_name"],
                            position=request.POST["cat_position"]
                        )

                        categories = category.objects.all()

                    return render(request, 'faq/question_edit.html',
                                  {'question_element': question_element,
                                   "categories": categories})
                elif 'edit_question' in request.POST:
                    cat = category.objects.get(name=request.POST["select_cat"])
                    question_element.question = request.POST["question"]
                    question_element.category = cat
                    question_element.position = request.POST[
                        "question_position"]
                    question_element.response = json.loads(
                        request.POST["editorjs_content"])
                    question_element.save()

                    return redirect(
                        reverse('faq_list',
                                kwargs={
                                    'message': 'The question is edited.'}
                                )
                    )

            else:
                return render(request, 'faq/question_edit.html',
                              {'question_element': question_element,
                               "categories": categories})
    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete category !"})


def question_confirm_delete(request, id_question):
    if request.user.is_authenticated and request.user.is_staff:
        question_element = question.objects.filter(id=id_question)

        if not question_element.exists():
            questions = question.objects.order_by('category__position',
                                                  'position')
            return render(request, 'faq/faq.html',
                          {'questions': questions})
        else:
            question_element = question.objects.get(id=id_question)

            return render(request, 'faq/question_confirm_delete.html',
                          {'question_element': question_element})
    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete category !"})


def question_delete(request, id_question):
    if request.user.is_authenticated and request.user.is_staff:
        question_element = question.objects.filter(id=id_question)

        if not question_element.exists():
            questions = question.objects.order_by('category__position',
                                                  'position')
            return render(request, 'faq/faq.html',
                          {'questions': questions})
        else:
            question_element = question.objects.get(id=id_question)
            cat = question_element.category.id
            question_element.delete()
            repos = question.objects.filter(category__id=cat).order_by(
                'position')

            compt = 1
            for r in repos:
                r.position = compt
                compt += 1
                r.save()

            return redirect(reverse('faq_list',
                                    kwargs={
                                        'message': 'Question is deleted !'}
                                    )
                            )

    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete question !"})


def question_add(request):
    if request.user.is_authenticated and request.user.is_staff:
        categories = category.objects.all()
        if request.method == "POST":
            if 'create_cat' in request.POST:
                cat = category.objects.filter(
                    name__iexact=request.POST["cat_name"])
                if not cat.exists():
                    category.objects.create(
                        name=request.POST["cat_name"],
                        position=request.POST["cat_position"]
                    )

                    categories = category.objects.all()

                return render(request, 'faq/faq_add.html',
                              {"categories": categories}
                              )
            elif 'create_question' in request.POST:
                cat = category.objects.get(name=request.POST["select_cat"])

                question.objects.create(
                    question=request.POST["question"],
                    category=cat,
                    position=request.POST["question_position"],
                    response=json.loads(request.POST["editorjs_content"])
                )

                repos = question.objects.filter(category=cat).order_by(
                    'position')

                compt = 1
                for r in repos:
                    r.position = compt
                    compt += 1
                    r.save()

                questions = question.objects.order_by('category__position',
                                                      'position')

                return render(request, 'faq/faq.html',
                              {'questions': questions,
                               'message_delete':
                                   'The question is saved.'
                               })

        else:
            return render(request, 'faq/faq_add.html',
                          {"categories": categories}
                          )
    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete question !"})


###############################################################################
# Manage Category
###############################################################################

def cat_modify(request, id_category):
    if request.user.is_authenticated and request.user.is_staff:
        cat = category.objects.get(id=id_category)
        if request.method == "POST":
            cat.name = request.POST["new_cat_name"]
            cat.position = request.POST["new_cat_position"]
            cat.save()

            questions = question.objects.order_by('category__position',
                                                  'position')
            return render(request, 'faq/faq.html',
                          {'questions': questions,
                           'message_saved':
                               'Category updated !'
                           })

        else:
            return render(request, 'faq/faq_modify_cat.html',
                          {"cat": cat}
                          )
    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete question !"})


def cat_confirm_delete(request, id_category):
    if request.user.is_authenticated and request.user.is_staff:
        cat = category.objects.filter(id=id_category)

        if not cat.exists():
            questions = question.objects.order_by('category__position',
                                                  'position')
            return render(request, 'faq/faq.html',
                          {'questions': questions})
        else:
            cat = category.objects.get(id=id_category)
            question_list = list(
                question.objects.filter(category__id=id_category).values_list(
                    "question", flat=True))

            return render(request, 'faq/cat_confirm_delete.html',
                          {'question_list': question_list,
                           'cat': cat})
    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete category !"})


def cat_delete(request, id_category):
    if request.user.is_authenticated and request.user.is_staff:
        cat = category.objects.filter(id=id_category)

        if not cat.exists():
            questions = question.objects.order_by('category__position',
                                                  'position')
            return render(request, 'faq/faq.html',
                          {'questions': questions})
        else:
            cat = category.objects.get(id=id_category)
            cat.delete()

            repos = category.objects.order_by('position')

            compt = 1
            for r in repos:
                r.position = compt
                compt += 1
                r.save()

            return redirect(reverse('faq_list',
                                    kwargs={
                                        'message': 'Category and associated '
                                                   'questions are deleted !'}))

    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete category !"})


def cat_remove_unused_confirm(request):
    if request.user.is_authenticated and request.user.is_staff:
        cat_unused = category.objects.filter(question=None)

        if not cat_unused.exists():
            questions = question.objects.order_by('category__position',
                                                  'position')
            return render(request, 'faq/faq.html',
                          {'questions': questions})
        else:

            return render(request, 'faq/cat_confirm_delete_unused.html',
                          {'cat_unused': cat_unused})
    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete category !"})


def cat_remove_unused(request):
    if request.user.is_authenticated and request.user.is_staff:
        cat_unused = category.objects.filter(question=None)

        if not cat_unused.exists():
            questions = question.objects.order_by('category__position',
                                                  'position')
            return render(request, 'faq/faq.html',
                          {'questions': questions})
        else:
            cat_unused.delete()

            repos = category.objects.order_by('position')

            compt = 1
            for r in repos:
                r.position = compt
                compt += 1
                r.save()

            return redirect(reverse('faq_list',
                                    kwargs={
                                        'message': 'Unused categories '
                                                   'are deleted !'}))

    else:
        questions = question.objects.order_by('category__position', 'position')
        return render(request, 'faq/faq.html',
                      {'questions': questions,
                       'message_staff':
                           "You must be staff to delete category !"})
