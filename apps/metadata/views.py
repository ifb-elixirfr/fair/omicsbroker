import json

from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import F
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.utils import timezone

from apps.metadata.models import metadata, checklist, \
    field, database
from apps.project.models import work_on, project


def metadata_create(request, id_project):
    if request.user.is_authenticated:
        if request.method == "POST":
            if 'validate_db' in request.POST:
                db_selected = get_object_or_404(database,
                                                id=request.POST[
                                                    "select_database_metadata"]
                                                )

                return render(request, 'metadata/metadata_create.html',
                              {'db_selected': db_selected,
                               'id_project': id_project})
            if 'validate_checklist' in request.POST:
                p = get_object_or_404(project, id=id_project)
                cl = get_object_or_404(checklist,
                                       id=request.POST["checklist_Id"])
                list_fields = field.objects.filter(checklist=cl)
                inter = {}
                for lf in list_fields:
                    inter[lf.name] = None

                inter_list = []
                for x in range(10):
                    inter_list.append(inter)

                m = metadata.objects.create(
                    project=p,
                    checklist=cl,
                    created_by=request.user.username,
                    last_modification_by=request.user.username,
                    short_description=request.POST["short_descrip"],
                    table=json.dumps(inter_list)
                )

                return redirect('metadata_detail',
                                id_metadata=m.id
                                )

        else:
            db_list = database.objects.all()
            return render(request, 'metadata/metadata_create.html',
                          {'db_list': db_list,
                           'id_project': id_project})
    else:
        return render(request, 'metadata/metadata_create.html',
                      {'message': "If you wish to create a metadata, "
                                  "you must be identified."})


def save_metadata(request, id_meta):
    metadata_element = get_object_or_404(metadata, id=id_meta)
    json_inter = json.loads(request.body)["data"]
    metadata_element.table = json.dumps(json_inter)

    fields = field.objects.filter(checklist=metadata_element.checklist)

    manda = 0
    reco = 0
    opt = 0

    col_manda = 0
    col_reco = 0
    col_opt = 0

    for f in fields:
        if f.requirement == 'mandatory':
            col_manda += 1
        elif f.requirement == 'recommended':
            col_reco += 1
        elif f.requirement == 'optional':
            col_opt += 1

        for item in json_inter:
            if item.get(f.name) is not None:
                if f.requirement == 'mandatory':
                    manda += 1
                if f.requirement == 'recommended':
                    reco += 1
                if f.requirement == 'optional':
                    opt += 1

    if col_manda != 0:
        metadata_element.mandatory = (manda / (
                    len(json_inter) * col_manda)) * 100

    if col_reco != 0:
        metadata_element.recommended = (reco / (
                    len(json_inter) * col_reco)) * 100
    if col_opt != 0:
        metadata_element.optional = (opt / (len(json_inter) * col_opt)) * 100

    metadata_element.metadata = json.dumps(
        json.loads(request.body)["metadata"]
    )
    metadata_element.last_modification_date = timezone.now()
    metadata_element.save()
    return HttpResponse('Ok')


def metadata_delete(request, id_metadata):
    if request.user.is_authenticated:
        metadata_element = get_object_or_404(metadata, id=id_metadata)

        r = work_on.objects.filter(
            applicationUser__user__username=request.user.username,
            project__id=metadata_element.project.id)

        if r.exists():
            metadata_element.delete()
            return HttpResponseRedirect(
                reverse("project_detail_message",
                        args=[metadata_element.project.id,
                              "The metadata are deleted"]))
        else:
            return HttpResponseRedirect(
                reverse("project_list_message",
                        args=[
                            "It's not your project !"]))
    else:
        return HttpResponseRedirect(
            reverse("project_list_message",
                    args=[
                        "If you wish to delete metadata, you must log in."]))


def metadata_detail(request, id_metadata):
    if request.user.is_authenticated:
        meta = get_object_or_404(metadata, id=id_metadata)
        r = work_on.objects.filter(
            applicationUser__user__username=request.user.username,
            project__id=meta.project.id)

        if r.exists():

            fields = metadata.objects.filter(id=id_metadata).values(
                "checklist__field__name").annotate(
                name=F('checklist__field__name'),
                description=F('checklist__field__description'),
                format=F('checklist__field__format'),
                restriction=F('checklist__field__restriction'),
                requirement=F('checklist__field__requirement'),
                example=F('checklist__field__example'),
            ).values(
                "name", "description", "format", "restriction", "requirement",
                "example"
            )

            return render(request, 'metadata/metadata_detail.html', context={
                'meta': meta,
                'fields': fields,
                'instruction': metadata.objects.get(
                    id=id_metadata
                ).checklist.instruction
            })

        else:
            return HttpResponseRedirect(
                reverse("project_list_message",
                        args=[
                            "It's not your project !"]))
    else:
        return HttpResponseRedirect(
            reverse("project_list_message",
                    args=[
                        "If you wish to delete metadata, you must log in."]))


def request_asJson_checklist(request, id_database):
    checklist_inter = checklist.objects.filter(
        database__id=id_database).values("id",
                                         "original_id",
                                         "name",
                                         "version",
                                         "description")

    structure = json.dumps(list(checklist_inter), cls=DjangoJSONEncoder)
    return HttpResponse(structure,
                        content_type='application/json;charset=utf-8')
