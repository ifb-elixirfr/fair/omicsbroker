from django.db import models
from django.utils import timezone

from apps.project.models import project


class database(models.Model):
    name = models.CharField(help_text="Name of database", max_length=50)

    url = models.URLField(help_text="URL database",
                          blank=True,
                          null=True)

    description = models.TextField(help_text="Description of database",
                                   blank=True,
                                   null=True)

    def __str__(self):
        return self.name


class checklist(models.Model):
    original_id = models.CharField(help_text="ID of checklist", max_length=50,
                                   blank=True,
                                   null=True)

    name = models.CharField(help_text="Name of checklist", max_length=50)

    version = models.CharField(help_text="Version of checklist", max_length=50,
                               blank=True, null=True)

    description = models.TextField(help_text="Description of checklist",
                                   blank=True,
                                   null=True)

    instruction = models.TextField(help_text="Checklist instructions",
                                   blank=True, null=True)

    url = models.URLField(help_text="URL checklist",
                          blank=True,
                          null=True)

    rawfile = models.FileField(help_text="RAW checklist file",
                               blank=True,
                               null=True)

    database = models.ForeignKey(database,
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class field(models.Model):
    REQUIREMENT = (
        ('recommended', 'recommended'),
        ('optional', 'optional'),
        ('mandatory', 'mandatory'),
    )

    name = models.CharField(help_text="Name of field", max_length=50)

    description = models.TextField(help_text="Name of field", max_length=50,
                                   blank=True, null=True)

    format = models.CharField(help_text="Name of field", max_length=50,
                              blank=True, null=True)

    restriction = models.TextField(help_text="Field restriction",
                                   blank=True, null=True)

    requirement = models.CharField(help_text="Field requirement",
                                   max_length=50, choices=REQUIREMENT,
                                   default='mandatory',
                                   blank=True, null=True)

    unit = models.TextField(help_text="Field unit",
                            blank=True, null=True)

    group = models.TextField(help_text="Field unit",
                             blank=True, null=True)

    example = models.TextField(help_text="Field example",
                               blank=True, null=True)

    checklist = models.ForeignKey(checklist,
                                  on_delete=models.CASCADE)


class metadata(models.Model):
    short_description = models.TextField(help_text="Short description",
                                         blank=True, null=True)
    table = models.JSONField(help_text="metadata table",
                             blank=True,
                             null=True,
                             )
    metadata = models.JSONField(help_text="metadata table",
                                blank=True,
                                null=True,
                                )
    created_date = models.DateTimeField(default=timezone.now)
    created_by = models.CharField(help_text="Created by XXX (Username)",
                                  max_length=50)
    last_modification_date = models.DateTimeField(default=timezone.now)
    last_modification_by = models.CharField(
        help_text="Last modification by XXX (Username)",
        max_length=50)
    project = models.ForeignKey(project, on_delete=models.CASCADE)
    checklist = models.ForeignKey(checklist, on_delete=models.CASCADE)
    mandatory = models.IntegerField(default=0)
    recommended = models.IntegerField(default=0)
    optional = models.IntegerField(default=0)
