import csv

from django.core.management.base import BaseCommand

from apps.metadata.models import database, checklist, field


class Command(BaseCommand):
    help = 'Create GISAID datatablse'

    def handle(self, *args, **kwargs):
        d = database.objects.create(
            name="GISAID",
            url="https://www.gisaid.org/",
            description="The GISAID Initiative promotes the rapid sharing of "
                        "data from all influenza viruses and the coronavirus "
                        "causing COVID-19. This includes genetic sequence and"
                        " related clinical and epidemiological data "
                        "associated with human viruses, and geographical "
                        "as well as species-specific data associated with "
                        "avian and other animal viruses, to help researchers "
                        "understand how viruses evolve and spread during "
                        "epidemics and pandemics."
        )

        cl = checklist.objects.create(
            name='EpiCoV hCoV-19 bulk upload',
            version='Version: 2021-02-24',
            description='GISAID hCoV-19 CLI Upload',
            instruction='''
Instructions:
- Enter your data into the sheet "Submissions"
- The mandatory columns are indicated in color.
- Do not change the content of the two first rows (1 & 2)
- Delete, overwrite the examples given in row 3
- your sequences must be in one single FASTA-File to compliment this
spreadsheet with your metadata
- EXCEL extension must remain .xls (not .xlsx). Always save in EXCEL 97 -
2003 Format.
- Provide for every row/virus the filename of the FASTA-File that contains
the corresponding sequence.
- "FASTA Filename" must match exactly the actual filename without any
directory prefixed. ("all_sequences.fasta" is OK,
"c:/users/meier/docs/all_sequences.fasta" is not)
- FASTA-Headers in the .FASTA-File must exactly match the values of
"Virus name" (e.g. >hCoV-19/Netherlands/Gelderland-01/2020)
- Do not change the type of the columns (Collection Date must be formatted
as "text" not "date")
- Always use the newest bulk-upload-XLS-Template
- Use "unknown" written in lower case if no value is available
- The user should name the XLS-Sheet as follows prior sending to the curation
team: "YYYYMMDD_a_descriptive_name_metadata.xls"
Upload your completed Excel sheet together with the FASTA-File through the
Batch Upload interface
In the event you experience any difficulties with your upload, please contact
us for assistance at hCoV-19@gisaid.org
What happens next?
EpiCoV Curators across different timezones will be alerted and review your
data. Only if necessary, will you be contacted, before your data are released
You will receive an eMail alert informing you that your data has been released.
''',
            database=d
        )

        with open('static-apps/checklists/GISAID.tsv') as tsvfile:
            spamreader = csv.reader(tsvfile, delimiter='\t')
            for row in spamreader:
                f = field()
                f.name = row[0]
                if row[1] == "":
                    f.requirement = 'optional'
                else:
                    f.requirement = row[1]
                if row[2].startswith('e.g.'):
                    f.example = row[2]
                else:
                    f.description = row[2]

                f.checklist = cl
                f.save()

        self.stdout.write("Done")
