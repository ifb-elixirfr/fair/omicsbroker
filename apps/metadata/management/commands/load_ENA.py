import glob

import xml.etree.ElementTree as ET

from django.core.management.base import BaseCommand

from apps.metadata.models import database, checklist, field


class Command(BaseCommand):
    help = 'Create GISAID datatablse'

    def handle(self, *args, **kwargs):
        d = database.objects.create(
            name="ENA",
            url="https://www.ebi.ac.uk/ena/browser/home",
            description="The European Nucleotide Archive (ENA) provides a "
                        "comprehensive record of the world’s nucleotide "
                        "sequencing information, covering raw sequencing "
                        "data, sequence assembly information and functional "
                        "annotation."
        )

        for file in glob.glob("static-apps/checklists/ENA/*.xml"):
            print(file)
            tree = ET.parse(file)
            root = tree.getroot()

            cl = checklist.objects.create(
                original_id=root.findall("./CHECKLIST/IDENTIFIERS/PRIMARY_ID")[
                    0].text,
                name=root.findall("./CHECKLIST/DESCRIPTOR/LABEL")[0].text,
                description=root.findall("./CHECKLIST/DESCRIPTOR/DESCRIPTION")[
                    0].text,
                url='https://www.ebi.ac.uk/ena/browser/view/' +
                    root.findall("./CHECKLIST/IDENTIFIERS/PRIMARY_ID")[0].text,
                rawfile=file,
                database=d
            )

            for fg in root.findall("./CHECKLIST/DESCRIPTOR/FIELD_GROUP"):
                for inter_field in fg.findall('FIELD'):
                    f = field()
                    f.name = inter_field.find('NAME').text

                    inter_split = inter_field.find('DESCRIPTION').text.split(
                        " Example: ")

                    f.description = inter_split[0]
                    f.format = inter_field.find('FIELD_TYPE')[0].tag
                    f.requirement = inter_field.find('MANDATORY').text

                    if inter_field.findall(
                            './FIELD_TYPE/TEXT_FIELD/REGEX_VALUE'):
                        f.restriction = inter_field.find(
                            './FIELD_TYPE/TEXT_FIELD/REGEX_VALUE').text
                    elif inter_field.findall(
                            './FIELD_TYPE/TEXT_CHOICE_FIELD/TEXT_VALUE'):
                        items = []
                        for i in inter_field.findall(
                                './FIELD_TYPE/TEXT_CHOICE_FIELD/TEXT_VALUE'):
                            items.append(i.find('VALUE').text)
                        f.restriction = "@".join(items)

                    if inter_field.findall(
                            './UNITS/UNIT'):
                        items = []
                        for i in inter_field.findall(
                                './UNITS/UNIT'):
                            items.append(i.text)
                        f.unit = "@".join(items)

                    f.group = fg.find('NAME').text

                    if len(inter_split) == 2:
                        f.example = inter_split[1]

                    f.checklist = cl

                    f.save()

        self.stdout.write("Done")
