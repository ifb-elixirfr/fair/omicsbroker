from django.urls import path

from apps.metadata.views import metadata_detail, save_metadata, \
    metadata_delete, request_asJson_checklist, \
    metadata_create

urlpatterns = [

    path('metadata/<int:id_metadata>', metadata_detail,
         name='metadata_detail'),

    path('metadata/add/to/<int:id_project>', metadata_create,
         name='metadata_create'),

    path('metadata/add/to/<int:id_project>/<int:id_checklist>',
         metadata_create,
         name='metadata_create'),

    path('metadata/<int:id_metadata>/delete/', metadata_delete,
         name='metadata_delete'),

    path('ajax_calls/save/metadata/table/<int:id_meta>', save_metadata,
         name="save_metadata"),

    path("json/checklist/<int:id_database>", request_asJson_checklist,
         name='request_asJson_checklist'),

]
