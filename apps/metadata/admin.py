from django.contrib import admin

from apps.metadata.models import checklist, database, field, metadata
from apps.home.admin import admin_site


@admin.register(database, site=admin_site)
class databaseAdmin(admin.ModelAdmin):
    save_on_top = True


@admin.register(checklist, site=admin_site)
class checklistAdmin(admin.ModelAdmin):
    save_on_top = True


@admin.register(field, site=admin_site)
class fieldAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = [f.name for f in field._meta.fields if f.name != "id"]


@admin.register(metadata, site=admin_site)
class metadataAdmin(admin.ModelAdmin):
    save_on_top = True
