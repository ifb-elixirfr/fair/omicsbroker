import json
from http import HTTPStatus

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from apps.countries.models import countries
from apps.metadata.models import metadata, database, checklist, field
from apps.metadata.templatetags.metadata_extras import remove_alpha, \
    remove_punct
from apps.project.models import project, work_on, tag, tag_project
from apps.users.models import applicationUser


class projectTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        p = project.objects.create(
            title="Great project",
            alias="great_project",
            description='a description')

        db = database.objects.create(
            name="toto"
        )

        cl = checklist.objects.create(
            name="titi",
            database=db
        )

        field.objects.create(
            name="field mandatory",
            checklist=cl,
            requirement="mandatory"
        )

        field.objects.create(
            name="field optional",
            checklist=cl,
            requirement="optional"
        )

        field.objects.create(
            name="field recommended",
            checklist=cl,
            requirement="recommended"
        )

        m = metadata()
        m.project = p
        m.checklist = cl
        m.save()

        u = User.objects.create_user(
            username='userProject',
            email='userProject@mail.com',
            password='djangoPWD')

        u2 = User.objects.create_user(
            username='userProject2',
            email='userProject2@mail.com',
            password='djangoPWD')

        u3 = User.objects.create_user(
            username='userProject3',
            email='userProject3@mail.com',
            password='djangoPWD')

        c = countries.objects.create(
            ADMIN="Aruba", ISO_A3="ABW",
            geometry='{ "type": "Feature", \
                    "properties": { "ADMIN": "Aruba", "ISO_A3": "ABW" }, \
                    "geometry": { "type": "Polygon",\
                    "coordinates": [ [ \
                    [ -69.996937628999916, 12.577582098000036 ], \
                    [ -69.936390753999945, 12.531724351000051 ],\
                    [ -69.924672003999945, 12.519232489000046 ],\
                    [ -69.915760870999918, 12.497015692000076 ],\
                    [ -69.880197719999842, 12.453558661000045 ],\
                    [ -69.876820441999939, 12.427394924000097 ],\
                    [ -69.888091600999928, 12.417669989000046 ],\
                    [ -69.908802863999938, 12.417792059000107 ],\
                    [ -69.930531378999888, 12.425970770000035 ],\
                    [ -69.945139126999919, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.447211005000014 ],\
                    [ -69.958566860999923, 12.463202216000099 ],\
                    [ -70.027658657999922, 12.522935289000088 ],\
                    [ -70.048085089999887, 12.531154690000079 ],\
                    [ -70.058094855999883, 12.537176825000088 ],\
                    [ -70.062408006999874, 12.546820380000057 ],\
                    [ -70.060373501999948, 12.556952216000113 ],\
                    [ -70.051096157999893, 12.574042059000064 ],\
                    [ -70.048736131999931, 12.583726304000024 ],\
                    [ -70.052642381999931, 12.600002346000053 ],\
                    [ -70.059641079999921, 12.614243882000054 ],\
                    [ -70.061105923999975, 12.625392971000068 ],\
                    [ -70.048736131999931, 12.632147528000104 ],\
                    [ -70.00715084499987, 12.5855166690001 ],\
                    [ -69.996937628999916, 12.577582098000036 ] ] ] } }')

        user_temp = applicationUser.objects.create(
            user=u,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        user_temp.save()

        user_temp2 = applicationUser.objects.create(
            user=u2,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        applicationUser.objects.create(
            user=u3,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        work_on.objects.create(
            applicationUser=user_temp,
            project=p,
            permission='All'
        )

        work_on.objects.create(
            applicationUser=user_temp2,
            project=p,
            permission='All'
        )

        t = tag.objects.create(
            label="test")

        tag_project.objects.create(
            tag=t,
            project=p)

    def test_metadata_detail_in(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        metadata_test = metadata.objects.all()[0]
        self.assertTrue(logged_in)
        url = reverse('metadata_detail',
                      kwargs={'id_metadata': metadata_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_metadata_detail_in_wrong_user(self):
        logged_in = self.client.login(username="userProject3",
                                      password="djangoPWD")
        metadata_test = metadata.objects.all()[0]
        self.assertTrue(logged_in)
        url = reverse('metadata_detail',
                      kwargs={'id_metadata': metadata_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

    def test_metadata_detail_out(self):
        metadata_test = metadata.objects.all()[0]
        url = reverse('metadata_detail',
                      kwargs={'id_metadata': metadata_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

    ###########################################################################
    # Model
    ###########################################################################

    def test_database_str(self):
        wp = database.objects.get(name="toto")
        self.assertEqual(str(wp), wp.name)

    def test_checklist_str(self):
        wp = checklist.objects.get(name="titi")
        self.assertEqual(str(wp), wp.name)

    ###########################################################################
    # Create
    ###########################################################################

    def test_metadata_create_in(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        url = reverse('metadata_create',
                      kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_metadata_create_out(self):
        project_test = project.objects.all()[0]
        url = reverse('metadata_create',
                      kwargs={'id_project': project_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_metadata_create_validate_db(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        db = database.objects.all()[0]

        url = reverse('metadata_create',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={"select_database_metadata": db.id,
                                          "validate_db":
                                              "validate_db"})

        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_metadata_create_validate_checklist(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        project_test = project.objects.all()[0]
        checklist_test = checklist.objects.all()[0]

        url = reverse('metadata_create',
                      kwargs={'id_project': project_test.id})
        response = self.client.post(url,
                                    data={
                                        "checklist_Id": checklist_test.id,
                                        "short_descrip": "A description",
                                        "validate_checklist":
                                            "validate_checklist"})

        testCreate = metadata.objects.filter(
            checklist__id=checklist_test.id,
            project__id=project_test.id).exists()

        self.assertTrue(testCreate)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    ###########################################################################
    # DELETE
    ###########################################################################

    def test_metadata_delete_in(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        p = project.objects.all()[0]
        db = database.objects.all()[0]
        m = metadata.objects.filter(
            project=p,
            checklist__database=db)

        test_exist = m.exists()
        self.assertTrue(test_exist)

        url = reverse('metadata_delete',
                      kwargs={'id_metadata': m[0].id})
        response = self.client.get(url)

        test_exist = metadata.objects.filter(
            project=p,
            checklist__database=db).exists()
        self.assertFalse(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_metadata_delete_out(self):
        p = project.objects.all()[0]
        db = database.objects.all()[0]
        m = metadata.objects.filter(
            project=p,
            checklist__database=db)

        test_exist = m.exists()
        self.assertTrue(test_exist)

        url = reverse('metadata_delete',
                      kwargs={'id_metadata': m[0].id})
        response = self.client.get(url)

        test_exist = metadata.objects.filter(
            project=p,
            checklist__database=db).exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_metadata_delete_bad_user(self):
        logged_in = self.client.login(username="userProject3",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        p = project.objects.all()[0]
        db = database.objects.all()[0]
        m = metadata.objects.filter(
            project=p,
            checklist__database=db)

        test_exist = m.exists()
        self.assertTrue(test_exist)

        url = reverse('metadata_delete',
                      kwargs={'id_metadata': m[0].id})
        response = self.client.get(url)

        test_exist = metadata.objects.filter(
            project=p,
            checklist__database=db).exists()
        self.assertTrue(test_exist)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    ###########################################################################
    # AJAX
    ###########################################################################

    def test_ajax_checklist(self):
        db = database.objects.all()[0]
        url = reverse('request_asJson_checklist',
                      kwargs={'id_database': db.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

        self.assertEqual(
            json.loads(response.content)[0]['name'],
            'titi'
        )

    def test_project_ajax_save_metadata(self):
        logged_in = self.client.login(username="userProject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)
        meta_test = metadata.objects.all()[0]
        url = reverse('save_metadata',
                      kwargs={'id_meta': meta_test.id})
        response = self.client.post(url,
                                    json.dumps(
                                        {'data': [{"field mandatory": "text 1",
                                                   "field optional": "text 2",
                                                   "field recommended":
                                                       "text 3"}],
                                         'metadata': [{'json': 'object'}]}),
                                    content_type="application/json")
        self.assertEqual(response.status_code, HTTPStatus.OK)

    ###########################################################################
    # EXTRAS
    ###########################################################################

    def test_remove_alpha(self):
        self.assertEqual(remove_alpha('toto@titi'), 'toto ; titi')
        self.assertEqual(remove_alpha(""), "")

    def test_remove_punct(self):
        self.assertEqual(remove_punct('toto!'), 'toto')
        self.assertEqual(remove_punct(""), "")
