import re

from django import template

register = template.Library()


@register.filter
def remove_alpha(value):
    if value:
        return value.replace("@", " ; ")
    else:
        return value


@register.filter
def remove_punct(value):
    if value:
        return re.sub(r'[^\w\s]', '', value).replace(" ", "")
    else:
        return value
