from django.urls import path

from apps.home.views import home_page
from apps.home.views import overview, request_asJson_Users, about, events, \
    wp_edit

urlpatterns = [
    path("", home_page, name="home_page"),
    path("overview/", overview, name="overview"),
    path("about/", about, name="about"),
    path("events/", events, name="events"),
    path("about/edit/<int:id_wb>", wp_edit, name="wp_edit"),
    path("tableStat/users/", request_asJson_Users,
         name='request_asJson_Users'),
]
