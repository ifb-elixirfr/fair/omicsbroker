from django.db import models


class web_page(models.Model):
    name = models.CharField(max_length=100, help_text="Web page name", )
    title = models.CharField(max_length=100, help_text="Web page name", )
    subTitle = models.CharField(max_length=100, help_text="Web page name", )
    content = models.JSONField(help_text="Web page content",)

    def __str__(self):
        return self.name
