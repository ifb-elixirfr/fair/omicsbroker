import json

from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse

from apps.project.models import project
from apps.users.models import applicationUser
from apps.home.models import web_page


def home_page(request):
    return render(request, "home/index.html")


def overview(request):
    count_users = applicationUser.objects.all().count()
    count_projects = project.objects.all().count()
    count_country = applicationUser.objects.values(
        "country").distinct().count()

    pie_data = [applicationUser.objects.filter(IFB_member="True").count(),
                applicationUser.objects.filter(IFB_member="False").count()]

    map_geometry = list(
        applicationUser.objects.values_list('country__geometry', flat=True))

    return render(request, 'home/overview.html',
                  {"count_users": count_users,
                   "count_projects": count_projects,
                   "count_country": count_country,
                   "pie_data": pie_data,
                   "map_geometry": map_geometry})


def request_asJson_Users(request):
    users_inter = applicationUser.objects.values("user__username",
                                                 "user__first_name",
                                                 "user__last_name",
                                                 "user__email")

    structure = json.dumps(list(users_inter), cls=DjangoJSONEncoder)
    return HttpResponse(structure,
                        content_type='application/json;charset=utf-8')


def about(request):
    wp = web_page.objects.filter(name="about")
    if not wp.exists():
        return render(request, 'home/wp.html',
                      {"message": "The page has not yet been created. "
                                  "Contact the website administrator"})
    else:
        wp = web_page.objects.get(name="about")
        return render(request, 'home/wp.html',
                      {"wp": wp})


def events(request):
    wp = web_page.objects.filter(name="events")
    if not wp.exists():
        return render(request, 'home/wp.html',
                      {"message": "The page has not yet been created. "
                                  "Contact the website administrator"})
    else:
        wp = web_page.objects.get(name="events")
        return render(request, 'home/wp.html',
                      {"wp": wp})


def wp_edit(request, id_wb):
    if request.user.is_authenticated and request.user.is_staff:
        wp = web_page.objects.filter(id=id_wb)
        if not wp.exists():
            return redirect(reverse('home_page'))
        else:
            wp = web_page.objects.get(id=id_wb)
            if request.method == "POST":
                if 'edit_wp' in request.POST:
                    wp.title = request.POST["wp_title"]
                    wp.subTitle = request.POST["wp_subTitle"]
                    wp.content = json.loads(
                        request.POST["editorjs_content"])
                    wp.save()

                    return redirect(reverse('home_page'))
            else:
                return render(request, 'home/wp_edit.html',
                              {'wp': wp})
    else:
        return redirect(reverse('home_page'))
