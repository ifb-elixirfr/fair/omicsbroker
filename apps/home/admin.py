from django.contrib import admin

from apps.home.models import web_page


class omicsBroker(admin.AdminSite):
    site_header = "omicsBroker administration"


admin_site = omicsBroker(name='admin')


@admin.register(web_page, site=admin_site)
class web_pageAdmin(admin.ModelAdmin):
    save_on_top = True
