import json
from http import HTTPStatus

from django.contrib.auth.models import User
from django.db.models import Max
from django.test import TestCase, Client
from django.urls import reverse

from apps.countries.models import countries
from apps.home.models import web_page
from apps.users.models import applicationUser


class TestPage(TestCase):

    def setUp(self):
        self.client = Client()

        u = User.objects.create_superuser(username='userStaff',
                                          first_name='user',
                                          last_name='Project',
                                          email='userStaff@mail.com',
                                          password='superPDW')

        u2 = User.objects.create_user(
            username='userNormal',
            email='userProject2@mail.com',
            password='djangoPWD')

        c = countries.objects.create(
            ADMIN="Aruba", ISO_A3="ABW",
            geometry='{ "type": "Feature", \
                        "properties": { "ADMIN": "Aruba", "ISO_A3": "ABW" }, \
                        "geometry": { "type": "Polygon",\
                        "coordinates": [ [ \
                        [ -69.996937628999916, 12.577582098000036 ], \
                        [ -69.936390753999945, 12.531724351000051 ],\
                        [ -69.924672003999945, 12.519232489000046 ],\
                        [ -69.915760870999918, 12.497015692000076 ],\
                        [ -69.880197719999842, 12.453558661000045 ],\
                        [ -69.876820441999939, 12.427394924000097 ],\
                        [ -69.888091600999928, 12.417669989000046 ],\
                        [ -69.908802863999938, 12.417792059000107 ],\
                        [ -69.930531378999888, 12.425970770000035 ],\
                        [ -69.945139126999919, 12.44037506700009 ],\
                        [ -69.924672003999945, 12.44037506700009 ],\
                        [ -69.924672003999945, 12.447211005000014 ],\
                        [ -69.958566860999923, 12.463202216000099 ],\
                        [ -70.027658657999922, 12.522935289000088 ],\
                        [ -70.048085089999887, 12.531154690000079 ],\
                        [ -70.058094855999883, 12.537176825000088 ],\
                        [ -70.062408006999874, 12.546820380000057 ],\
                        [ -70.060373501999948, 12.556952216000113 ],\
                        [ -70.051096157999893, 12.574042059000064 ],\
                        [ -70.048736131999931, 12.583726304000024 ],\
                        [ -70.052642381999931, 12.600002346000053 ],\
                        [ -70.059641079999921, 12.614243882000054 ],\
                        [ -70.061105923999975, 12.625392971000068 ],\
                        [ -70.048736131999931, 12.632147528000104 ],\
                        [ -70.00715084499987, 12.5855166690001 ],\
                        [ -69.996937628999916, 12.577582098000036 ] ] ] } }')

        applicationUser.objects.create(
            user=u,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        applicationUser.objects.create(
            user=u2,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        web_page.objects.create(
            name="test",
            title="Test",
            subTitle="Sub test",
            content="Hello"
        )

    def test_index_page(self):
        url = reverse("home_page")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_about_page(self):
        web_page.objects.create(
            name="about",
            title="About",
            subTitle="About us, ...",
            content="Hello"
        )
        url = reverse("about")
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'home/wp.html')
        self.assertEqual(response.status_code, 200)

    def test_about_page_not_exist(self):
        url = reverse("about")
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'home/wp.html')
        self.assertEqual(response.status_code, 200)

    def test_events_page(self):
        web_page.objects.create(
            name="events",
            title="Events",
            subTitle="Our events",
            content='{"foo": "bar"}'
        )
        url = reverse("events")
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'home/wp.html')
        self.assertEqual(response.status_code, 200)

    def test_event_page_not_exist(self):
        url = reverse("events")
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'home/wp.html')
        self.assertEqual(response.status_code, 200)

    def test_overview_page(self):
        url = reverse("overview")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_ajax_users(self):
        url = reverse('request_asJson_Users')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

        self.assertEqual(
            json.loads(response.content)[0]['user__username'],
            'userStaff'
        )
        self.assertEqual(
            json.loads(response.content)[0]['user__first_name'],
            'user'
        )
        self.assertEqual(
            json.loads(response.content)[0]['user__last_name'],
            'Project'
        )
        self.assertEqual(
            json.loads(response.content)[0]['user__email'],
            'userStaff@mail.com'
        )

    def test_web_page_str(self):
        wp = web_page.objects.get(name="test")
        self.assertEqual(str(wp), wp.name)

    # =========================================================================
    # Edit web page
    # =========================================================================

    def test_wp_edit_logout(self):
        wp = web_page.objects.get(name="test")
        url = reverse('wp_edit',
                      kwargs={'id_wb': wp.id}
                      )
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_wp_edit_login_no_staff(self):
        logged_in = self.client.login(username="userNormal",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        wp = web_page.objects.get(name="test")
        url = reverse('wp_edit',
                      kwargs={'id_wb': wp.id}
                      )
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_wp_edit_login_staff_get(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        wp = web_page.objects.get(name="test")
        url = reverse('wp_edit',
                      kwargs={'id_wb': wp.id}
                      )
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_wp_edit_login_staff_get_no_exist(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        random_id = web_page.objects.aggregate(Max('id'))['id__max'] + 1

        url = reverse('wp_edit',
                      kwargs={'id_wb': random_id}
                      )
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_wp_edit_login_staff_post(self):
        logged_in = self.client.login(username="userStaff",
                                      password="superPDW")
        self.assertTrue(logged_in)

        wp = web_page.objects.get(name="test")
        url = reverse('wp_edit',
                      kwargs={'id_wb': wp.id}
                      )
        response = self.client.post(url,
                                    data={"wp_title": "toto",
                                          "wp_subTitle": "titi",
                                          "editorjs_content": '{"foo": "bar"}',
                                          "edit_wp": "edit_wp"})

        test_exist = web_page.objects.filter(title="toto").exists()
        self.assertTrue(test_exist)

        test_exist = web_page.objects.filter(title="Test").exists()
        self.assertFalse(test_exist)

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
