from django.contrib.auth import logout as django_logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from apps.users.forms import applicationUserForm, UserForm
from apps.countries.models import countries
from apps.users.models import applicationUser


def sign_up(request):
    error = False
    country_list = countries.objects.values_list('ADMIN', flat=True)
    if request.method == "POST":
        u_form = UserForm(request.POST)
        p_form = applicationUserForm(request.POST, data_list=country_list)

        updated_request = request.POST.copy()

        # Chek if country if valid and update Request
        if countries.objects.filter(ADMIN=p_form.data['country']).exists():
            updated_request.update({'country': countries.objects.get(
                ADMIN=p_form.data['country'])})
        else:
            updated_request.update({'country': None})

        p_form = applicationUserForm(updated_request,
                                     data_list=country_list)

        if u_form.is_valid():
            user = u_form.save(commit=False)

            if p_form.is_valid():
                user = u_form.save()
                p_form = p_form.save(commit=False)
                p_form.user = user
                p_form.save()
                return HttpResponseRedirect(
                    reverse('thanks', args=[user.username]))
            else:
                error = True
        else:
            error = True
    else:
        u_form = UserForm(request.POST)
        p_form = applicationUserForm(request.POST, data_list=country_list)

    if error:
        updated_request.update({'country': None})
        p_form = applicationUserForm(updated_request,
                                     data_list=country_list)

    return render(request, 'users/signUp.html',
                  {'u_form': u_form, 'p_form': p_form, 'error': error})


def thanks(request, userName):
    return render(request, 'users/thanks.html', context={
        'userName': userName,
    })


def profile(request):
    if request.user.is_authenticated:
        app_user = applicationUser.objects.get(
            user__username=request.user.username)
        return render(request, 'users/profile.html',
                      {'app_user': app_user})
    else:
        return HttpResponseRedirect(reverse('login'))


@login_required
def logout(request):
    django_logout(request)
    return HttpResponseRedirect('/')
