from django.urls import path, include

from apps.users.views import sign_up, thanks, logout, profile

urlpatterns = [
    path("signUp/", sign_up, name='signUp'),
    path('accounts/', include('django.contrib.auth.urls')),
    path("profile/", profile, name='profile'),
    path("logout/", logout, name='logout'),
    path("thanks/<str:userName>/", thanks, name='thanks'),
]
