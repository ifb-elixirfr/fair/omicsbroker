from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


from apps.users.fields import ListTextWidget
from apps.users.models import applicationUser


class UserForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email',
                  'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class applicationUserForm(forms.ModelForm):
    class Meta:
        model = applicationUser
        fields = ['IFB_member', 'country', 'location', 'position']

    def __init__(self, *args, **kwargs):
        _country_list = kwargs.pop('data_list', None)
        super(applicationUserForm, self).__init__(*args, **kwargs)
        self.fields['country'].widget = ListTextWidget(data_list=_country_list,
                                                       name='country-list')
        for visible in self.visible_fields():
            if visible.field.widget.input_type != 'checkbox':
                visible.field.widget.attrs['class'] = 'form-control'
