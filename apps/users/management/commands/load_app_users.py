import csv
from distutils.util import strtobool

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from apps.countries.models import countries
from apps.users.models import applicationUser


class Command(BaseCommand):
    help = 'Reload regions data'

    def handle(self, *args, **kwargs):
        with open('apps/users/static/data/applicationUsers.csv',
                  newline='', encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
            next(spamreader, None)
            for row in spamreader:
                u = User.objects.create_user(
                    username=row[0],
                    first_name=row[1],
                    last_name=row[2],
                    email=row[3],
                    password=row[4],
                    is_superuser=strtobool(row[5]),
                    is_staff=strtobool(row[6])
                )
                c = countries.objects.get(ADMIN=row[8])
                applicationUser.objects.create(
                    user=u,
                    country=c,
                    IFB_member=strtobool(row[7]),
                    location=row[9],
                    position=row[10]
                )

        self.stdout.write("Done")
