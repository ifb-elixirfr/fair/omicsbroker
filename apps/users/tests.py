from http import HTTPStatus

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from apps.users.models import applicationUser
from apps.countries.models import countries

from apps.users.admin import UserAdmin


class UsersTests(TestCase):

    def setUp(self):
        u = User.objects.create(
            username='toto',
            email='toto@user.com',
            password='superPDW')
        c = countries.objects.create(
            ADMIN="Aruba", ISO_A3="ABW",
            geometry='{ "type": "Feature", \
            "properties": { "ADMIN": "Aruba", "ISO_A3": "ABW" }, \
            "geometry": { "type": "Polygon",\
            "coordinates": [ [ \
            Ò[ -69.996937628999916, 12.577582098000036 ], \
            [ -69.936390753999945, 12.531724351000051 ],\
            [ -69.924672003999945, 12.519232489000046 ],\
            [ -69.915760870999918, 12.497015692000076 ],\
            [ -69.880197719999842, 12.453558661000045 ],\
            [ -69.876820441999939, 12.427394924000097 ],\
            [ -69.888091600999928, 12.417669989000046 ],\
            [ -69.908802863999938, 12.417792059000107 ],\
            [ -69.930531378999888, 12.425970770000035 ],\
            [ -69.945139126999919, 12.44037506700009 ],\
            [ -69.924672003999945, 12.44037506700009 ],\
            [ -69.924672003999945, 12.447211005000014 ],\
            [ -69.958566860999923, 12.463202216000099 ],\
            [ -70.027658657999922, 12.522935289000088 ],\
            [ -70.048085089999887, 12.531154690000079 ],\
            [ -70.058094855999883, 12.537176825000088 ],\
            [ -70.062408006999874, 12.546820380000057 ],\
            [ -70.060373501999948, 12.556952216000113 ],\
            [ -70.051096157999893, 12.574042059000064 ],\
            [ -70.048736131999931, 12.583726304000024 ],\
            [ -70.052642381999931, 12.600002346000053 ],\
            [ -70.059641079999921, 12.614243882000054 ],\
            [ -70.061105923999975, 12.625392971000068 ],\
            [ -70.048736131999931, 12.632147528000104 ],\
            [ -70.00715084499987, 12.5855166690001 ],\
            [ -69.996937628999916, 12.577582098000036 ] ] ] } }')

        applicationUser.objects.create(
            user=u,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

    def test_appUser(self):
        app_user = applicationUser.objects.all()[0]
        self.assertEqual("IFB", app_user.location)
        self.assertEqual("toto", app_user.user.username)


class UsersTests_adminView(TestCase):

    def setUp(self):
        self.my_admin = User.objects.create_superuser('myuser',
                                                      'myemail@test.com',
                                                      "password")

    def test_changelist_view(self):
        self.client.login(username=self.my_admin.username, password="password")
        url = reverse('admin:auth_user_changelist')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class UsersTests_admin(TestCase):

    def setUp(self):
        self.client = Client()
        u = User.objects.create(
            username='superToto',
            email='superToto@user.com',
            password='superPDW',
            is_superuser=True,
            is_staff=True
        )
        c = countries.objects.create(
            ADMIN="Aruba", ISO_A3="ABW",
            geometry='{ "type": "Feature",\
            "properties": { "ADMIN": "Aruba", "ISO_A3": "ABW" }, \
            "geometry": { "type": "Polygon",\
            "coordinates": [ [ \
            [ -69.996937628999916, 12.577582098000036 ], \
            [ -69.936390753999945, 12.531724351000051 ],\
            [ -69.924672003999945, 12.519232489000046 ],\
            [ -69.915760870999918, 12.497015692000076 ],\
            [ -69.880197719999842, 12.453558661000045 ],\
            [ -69.876820441999939, 12.427394924000097 ],\
            [ -69.888091600999928, 12.417669989000046 ],\
            [ -69.908802863999938, 12.417792059000107 ],\
            [ -69.930531378999888, 12.425970770000035 ],\
            [ -69.945139126999919, 12.44037506700009 ],\
            [ -69.924672003999945, 12.44037506700009 ],\
            [ -69.924672003999945, 12.447211005000014 ],\
            [ -69.958566860999923, 12.463202216000099 ],\
            [ -70.027658657999922, 12.522935289000088 ],\
            [ -70.048085089999887, 12.531154690000079 ],\
            [ -70.058094855999883, 12.537176825000088 ],\
            [ -70.062408006999874, 12.546820380000057 ],\
            [ -70.060373501999948, 12.556952216000113 ],\
            [ -70.051096157999893, 12.574042059000064 ],\
            [ -70.048736131999931, 12.583726304000024 ],\
            [ -70.052642381999931, 12.600002346000053 ],\
            [ -70.059641079999921, 12.614243882000054 ],\
            [ -70.061105923999975, 12.625392971000068 ],\
            [ -70.048736131999931, 12.632147528000104 ],\
            [ -70.00715084499987, 12.5855166690001 ],\
            [ -69.996937628999916, 12.577582098000036 ] ] ] } }')

        applicationUser.objects.create(
            user=u,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

    def test_admin_str(self):
        app_user = applicationUser.objects.all()[0]
        self.assertEquals(str(app_user), app_user.user.username)

    def test_admin_location(self):
        app_user = applicationUser.objects.all()[0]
        admin_function_result = UserAdmin.get_location(self, app_user)
        self.assertEquals(admin_function_result, app_user.location)

    def test_admin_country(self):
        app_user = applicationUser.objects.all()[0]
        admin_function_result = UserAdmin.get_country(self, app_user)
        self.assertEquals(admin_function_result, app_user.country)

    def test_admin_ifb(self):
        app_user = applicationUser.objects.all()[0]
        admin_function_result = UserAdmin.get_ifb(self, app_user)
        self.assertEquals(admin_function_result, app_user.IFB_member)

    def test_admin_position(self):
        app_user = applicationUser.objects.all()[0]
        admin_function_result = UserAdmin.get_position(self, app_user)
        self.assertEquals(admin_function_result, app_user.position)


class TestPage(TestCase):
    def setUp(self):
        self.client = Client()
        countries.objects.create(
            ADMIN="Aruba", ISO_A3="ABW",
            geometry='{ "type": "Feature",\
                    "properties": { "ADMIN": "Aruba", "ISO_A3": "ABW" }, \
                    "geometry": { "type": "Polygon",\
                    "coordinates": [ [ \
                    [ -69.996937628999916, 12.577582098000036 ], \
                    [ -69.936390753999945, 12.531724351000051 ],\
                    [ -69.924672003999945, 12.519232489000046 ],\
                    [ -69.915760870999918, 12.497015692000076 ],\
                    [ -69.880197719999842, 12.453558661000045 ],\
                    [ -69.876820441999939, 12.427394924000097 ],\
                    [ -69.888091600999928, 12.417669989000046 ],\
                    [ -69.908802863999938, 12.417792059000107 ],\
                    [ -69.930531378999888, 12.425970770000035 ],\
                    [ -69.945139126999919, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.447211005000014 ],\
                    [ -69.958566860999923, 12.463202216000099 ],\
                    [ -70.027658657999922, 12.522935289000088 ],\
                    [ -70.048085089999887, 12.531154690000079 ],\
                    [ -70.058094855999883, 12.537176825000088 ],\
                    [ -70.062408006999874, 12.546820380000057 ],\
                    [ -70.060373501999948, 12.556952216000113 ],\
                    [ -70.051096157999893, 12.574042059000064 ],\
                    [ -70.048736131999931, 12.583726304000024 ],\
                    [ -70.052642381999931, 12.600002346000053 ],\
                    [ -70.059641079999921, 12.614243882000054 ],\
                    [ -70.061105923999975, 12.625392971000068 ],\
                    [ -70.048736131999931, 12.632147528000104 ],\
                    [ -70.00715084499987, 12.5855166690001 ],\
                    [ -69.996937628999916, 12.577582098000036 ] ] ] } }')

    def test_thanks_page(self):
        url = reverse('thanks', args=["toto"])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/thanks.html')
        self.assertContains(response, 'toto')

    def test_sign_up_page_get(self):
        url = reverse('signUp')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/signUp.html')

    def test_sign_up_page_post(self):
        url = reverse('signUp')
        response = self.client.post(
            url,
            data={"username": "toto",
                  "first_name": "tata",
                  "last_name": "titi",
                  "email": "toto@mail.com",
                  "password1": "djangoPWD",
                  "password2": "djangoPWD",
                  "IFB_member": "False",
                  "country": "Aruba",
                  "location": "Paris",
                  "position": "worker"}
        )

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response["Location"],
                         reverse('thanks', args=["toto"]))

    def test_sign_up_page_post_bad_country(self):
        url = reverse('signUp')
        response = self.client.post(
            url,
            data={"username": "toto",
                  "first_name": "tata",
                  "last_name": "titi",
                  "email": "toto@mail.com",
                  "password1": "djangoPWD",
                  "password2": "djangoPWD",
                  "IFB_member": "False",
                  "country": "badValue",
                  "location": "Paris",
                  "position": "worker"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "users/signUp.html")

    def test_sign_up_page_post_bad_pwd(self):
        url = reverse('signUp')
        response = self.client.post(
            url,
            data={"username": "toto",
                  "first_name": "tata",
                  "last_name": "titi",
                  "email": "toto@mail.com",
                  "password1": "djangoPWD",
                  "password2": "djangopwd",
                  "IFB_member": "False",
                  "country": "Aruba",
                  "location": "Paris",
                  "position": "worker"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "users/signUp.html")


class UsersTests_log(TestCase):

    def setUp(self):
        user = User.objects.create(username='toto')
        user.set_password('djangoPWD')
        user.save()

    def test_login(self):
        logged_in = self.client.login(username='toto', password='djangoPWD')
        self.assertTrue(logged_in)

    def test_login_bad(self):
        url = reverse('login')
        response = self.client.post(url, data={
            "username": "titi",
            "password": "badPWD"})

        self.assertNotEqual(response.status_code, HTTPStatus.FOUND)
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_logout(self):
        self.client.login(username='toto', password='djangoPWD')
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, 302)

    def test_sign_up_page_get(self):
        url = reverse('login')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')


class profileTestCase(TestCase):
    def setUp(self):
        self.client = Client()

        u = User.objects.create_user(
            username='userProfile',
            email='userProject@mail.com',
            password='djangoPWD')

        c = countries.objects.create(
            ADMIN="Aruba", ISO_A3="ABW",
            geometry='{ "type": "Feature", \
                    "properties": { "ADMIN": "Aruba", "ISO_A3": "ABW" }, \
                    "geometry": { "type": "Polygon",\
                    "coordinates": [ [ \
                    Ò[ -69.996937628999916, 12.577582098000036 ], \
                    [ -69.936390753999945, 12.531724351000051 ],\
                    [ -69.924672003999945, 12.519232489000046 ],\
                    [ -69.915760870999918, 12.497015692000076 ],\
                    [ -69.880197719999842, 12.453558661000045 ],\
                    [ -69.876820441999939, 12.427394924000097 ],\
                    [ -69.888091600999928, 12.417669989000046 ],\
                    [ -69.908802863999938, 12.417792059000107 ],\
                    [ -69.930531378999888, 12.425970770000035 ],\
                    [ -69.945139126999919, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.44037506700009 ],\
                    [ -69.924672003999945, 12.447211005000014 ],\
                    [ -69.958566860999923, 12.463202216000099 ],\
                    [ -70.027658657999922, 12.522935289000088 ],\
                    [ -70.048085089999887, 12.531154690000079 ],\
                    [ -70.058094855999883, 12.537176825000088 ],\
                    [ -70.062408006999874, 12.546820380000057 ],\
                    [ -70.060373501999948, 12.556952216000113 ],\
                    [ -70.051096157999893, 12.574042059000064 ],\
                    [ -70.048736131999931, 12.583726304000024 ],\
                    [ -70.052642381999931, 12.600002346000053 ],\
                    [ -70.059641079999921, 12.614243882000054 ],\
                    [ -70.061105923999975, 12.625392971000068 ],\
                    [ -70.048736131999931, 12.632147528000104 ],\
                    [ -70.00715084499987, 12.5855166690001 ],\
                    [ -69.996937628999916, 12.577582098000036 ] ] ] } }')

        user_temp = applicationUser.objects.create(
            user=u,
            country=c,
            location="IFB",
            IFB_member="True",
            position="worker"
        )

        user_temp.save()

    def test_login_profile(self):
        logged_in = self.client.login(username='userProfile',
                                      password='djangoPWD')
        self.assertTrue(logged_in)
        url = reverse('profile')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'users/profile.html')

    def test_login_profile_no_log(self):
        url = reverse('profile')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
