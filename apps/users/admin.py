import json

from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Count
from django.db.models.functions import TruncDay

from apps.home.admin import admin_site
from apps.users.models import applicationUser


class applicationUserInline(admin.TabularInline):
    model = applicationUser
    extra = 1
    max_num = 1


admin.site.unregister(User)


@admin.register(User, site=admin_site)
class UserAdmin(admin.ModelAdmin):
    save_on_top = True
    search_fields = ['username', 'first_name', 'last_name']

    list_display = ('username', 'first_name', 'last_name', 'email',
                    'get_country', 'get_location', 'get_position',
                    'get_ifb')

    list_filter = ['applicationuser__IFB_member']

    inlines = [applicationUserInline]

    def get_location(self, obj):
        return applicationUser.objects.get(user=obj.pk).location

    def get_country(self, obj):
        return applicationUser.objects.get(user=obj.pk).country

    def get_position(self, obj):
        return applicationUser.objects.get(user=obj.pk).position

    def get_ifb(self, obj):
        return applicationUser.objects.get(user=obj.pk).IFB_member

    get_ifb.boolean = True

    get_location.short_description = 'Location'
    get_country.short_description = 'Country'
    get_position.short_description = 'Position'
    get_ifb.short_description = 'IFB_member'
    change_list_template = 'admin/userAdmin.html'

    def changelist_view(self, request, extra_context=None):
        # New users per day
        chart_data = (
            User.objects.annotate(date=TruncDay("date_joined"))
                .values("date")
                .annotate(y=Count("id"))
                .order_by("-date")
        )
        as_json_user = json.dumps(list(chart_data), cls=DjangoJSONEncoder)

        # IFB member ?
        pie_data = list(User.objects.values("applicationuser__IFB_member")
                        .filter(applicationuser__IFB_member="True")
                        .annotate(y=Count("applicationuser__IFB_member"))
                        .values_list('y', flat=True)) + \
            list(User.objects.values("applicationuser__IFB_member")
                 .filter(applicationuser__IFB_member="False")
                 .annotate(y=Count("applicationuser__IFB_member"))
                 .values_list('y', flat=True))

        extra_context = extra_context or {"chart_data": as_json_user,
                                          "pie_data": pie_data}

        # Call the superclass changelist_view to render the page
        return super().changelist_view(request, extra_context=extra_context)


admin_site.register(Group)
