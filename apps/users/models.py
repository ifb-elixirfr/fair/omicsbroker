from django.contrib.auth.models import User
from django.db import models

from apps.countries.models import countries


class applicationUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    IFB_member = models.BooleanField(help_text="Are you member of IFB ?",
                                     default=False)
    country = models.ForeignKey(countries, on_delete=models.CASCADE)
    location = models.CharField(max_length=500, help_text="Your laboratory", )
    position = models.CharField(max_length=500, help_text="Your position", )

    def __str__(self):
        return self.user.username
