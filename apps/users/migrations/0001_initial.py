# Generated by Django 3.1.6 on 2021-03-30 07:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('countries', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='applicationUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('IFB_member', models.BooleanField(default=False, help_text='Are you member of IFB ?')),
                ('location', models.CharField(help_text='Your laboratory', max_length=500)),
                ('position', models.CharField(help_text='Your position', max_length=500)),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='countries.countries')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
