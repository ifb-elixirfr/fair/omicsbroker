# Generated by Django 3.1.6 on 2021-03-30 07:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='countries',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ADMIN', models.CharField(help_text='The common name for the country', max_length=100)),
                ('ISO_A3', models.CharField(help_text='ISO_A3 code', max_length=3)),
                ('geometry', models.JSONField()),
            ],
        ),
    ]
