from django.db import models


class countries(models.Model):
    ADMIN = models.CharField(max_length=100,
                             help_text="The common name for the country")
    ISO_A3 = models.CharField(max_length=3, help_text="ISO_A3 code")
    geometry = models.JSONField()

    def __str__(self):
        return self.ADMIN
