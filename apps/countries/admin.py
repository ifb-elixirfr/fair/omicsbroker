from django.contrib import admin

from apps.countries.models import countries
from apps.home.admin import admin_site


@admin.register(countries, site=admin_site)
class countriesAdmin(admin.ModelAdmin):
    save_on_top = True
    search_fields = ['ADMIN']

    list_display = ('ADMIN', 'ISO_A3')
