from django.urls import path, include

from apps.home.admin import admin_site

urlpatterns = [
    path('admin/', admin_site.urls),
    path("", include("apps.home.urls")),
    path("", include("apps.faq.urls")),
    path("", include("apps.users.urls")),
    path("", include("apps.project.urls")),
    path("", include("apps.metadata.urls")),
]
