# omicsBroker

omicsBroker is a tool to easily annotate and publish omics data to international databases.

**Language**

[![Made with Django](https://img.shields.io/badge/Made%20with-Django-blue)](https://www.djangoproject.com/)
[![Made with JS](https://img.shields.io/badge/Made%20with-JS-blue)](https://gitlab.com/ifb-elixirfr/fair/omicsbroker/-/commits/master)
[![Made with Bootstrap](https://img.shields.io/badge/Made%20with-Bootstrap-blue)](https://getbootstrap.com/)

**Continuous Integration**

[![pipeline status](https://gitlab.com/ifb-elixirfr/fair/omicsbroker/badges/master/pipeline.svg)](https://gitlab.com/ifb-elixirfr/fair/omicsbroker/-/commits/master)
[![Coverage](https://gitlab.com/ifb-elixirfr/fair/omicsbroker/-/jobs/artifacts/master/raw/coverage.svg?job=docker-test-coverage-master)](https://gitlab.com/ifb-elixirfr/fair/omicsbroker/)
[![Flake8](https://gitlab.com/ifb-elixirfr/fair/omicsbroker/-/jobs/artifacts/master/raw/flake8.svg?job=docker-test-flake8-master)](https://flake8.pycqa.org/en/latest/)

**Questions**

[:speech_balloon: Ask a question](https://gitlab.com/ifb-elixirfr/fair/omicsbroker/-/issues/new)
[:book: Lisez les questions](https://gitlab.com/ifb-elixirfr/fair/omicsbroker/-/issues)
[:e-mail: Par mail](mailto:thomas.denecker@gfrance-bioinformatique.fr)

**Contribution**

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)

## Docker

The omicsBroker application was developed with Docker and the image containing R and all necessary libraries is available online ([here](https://hub.docker.com/repository/docker/tdenecker/omicsbroker)).
Using omicsBroker by this method guarantees reproducibility of the analyses.

### Requirements

We use Docker to develop and manage omicsBroker. We invite you to verify that the
following requirements are correctly satisfied before trying to bootstrap the
application:

* [Docker 3.1.0+](https://docs.docker.com/get-docker/)

> We recommend you to follow Docker's official documentations to install
required docker tools (see links above).To help you, explanatory videos for each
operating system are available [here](https://www.bretfisher.com/installdocker/)

**Docker must be on for the duration of omicsBroker use.**

## Quick start

Have you read the "Requirements" section above?

### Run application

**Reminder** : Docker must always be switched on for any installation and use of omicsBroker !

**IMPORTANT** : For linux users, it is necessary to add `sudo` before all `dock` commands.

#### Step 1 : Open a terminal

- [Windows](https://youtu.be/uE9WgNr3OjM)
- [mac OSX](https://www.youtube.com/watch?v=QROX039ckO8)
- [Linux](https://linuxconfig.org/how-to-open-a-terminal-on-ubuntu-bionic-beaver-18-04-linux)

#### Step 2 : Clone project

``` bash
git clone https://gitlab.com/ifb-elixirfr/fair/omicsbroker.git
cd omicsbroker
```

#### Step 3: Run docker compose

``` bash
# Download last app image
docker-compose -f docker-compose.prod.yml pull web

# Run images
docker-compose -f docker-compose.prod.yml up -d

# Migrate models into database 
docker-compose -f docker-compose.prod.yml exec web python manage.py migrate 

# Create super user
docker-compose -f docker-compose.prod.yml exec web python manage.py createsuperuser

# Populate 
docker-compose -f docker-compose.prod.yml exec web python manage.py load_countries

# Get static file
docker-compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input --clear

```

*Note* : You can populate your database with dummy data using the command :
``` bash
bash populate_exemple_production.sh 
```

#### Step 4: Open your favorite web browser and play with omicsBroker

omicsBroker is running. You can open a web browser and use it in the following url : [http://localhost:443/](http://localhost:443/).

##### Step 5 : Close omicsBroker

``` bash
docker-compose -f docker-compose.prod.yml down
```

##### Step 6 : Restart

``` bash
docker-compose -f docker-compose.prod.yml up -d
```

##### Step 7 : Get logs

##### Production
``` bash
docker-compose -f docker-compose.prod.yml logs -f
```

## Specificity for deployment on a server

### 1- Update `.env.prod` file

- Change the secret key. You can use a generator like : https://djecrety.ir/
- Change DJANGO_ALLOWED_HOSTS if you work on a server. If you work on your personal computer, do not modify

### 2- Secure your connection

If you are working on a server, it is recommended to secure the connection. An example is given below. 


### 3- Create SSL certificates

A certificat file (.crt) and a certificate key file (.key). To generate these certificates, 
we advise you to contact the administrator of your server. If you use a hosting provider,
the documentation is generally very detailed for this type of case. 
Finally, if you do not use a hosting provider, a domain name is mandatory for 
the generation of SSL certificates and we recommend that you follow the [letsencrypt](https://letsencrypt.org/fr/) reminder.

### 4- Copy file in `nginx` folder in omicsBroker

After, copy the two generated file in `nginx` folder :

``` bash
cp /etc/ssl/private/omicsBroker.key nginx/
cp /etc/ssl/certs/omicsBroker.crt nginx/
```

Now, in the `nginx` folder, you have 2 new files : `omicsBroker.key` and `omicsBroker.crt`.

### 5- Update Nginx files

**1- Dockerfile**

Add these lines :

``` bash
COPY omicsBroker.crt /etc/ssl/certs/
COPY omicsBroker.key /etc/ssl/private/
```

You have now :

``` bash
FROM nginx:1.19.0-alpine

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
COPY omicsBroker.crt /etc/ssl/certs/
COPY omicsBroker.key /etc/ssl/private/
```

**2- Update `nginx.conf` file**

Replace `XXXXXXXXXXXXXXXXXXXXXXXXXXX` by your server name (or a IP)
``` bash
upstream omicsBroker {
    server web:8000;
}

server {
    listen 80;
    listen [::]:80;
    server_name XXXXXXXXXXXXXXXXXXXXXXXXXXX ;
    return 301 https://XXXXXXXXXXXXXXXXXXXXXXXXXXX$request_uri;
}


server {

    listen 443 ssl ;
    listen [::]:443 ssl;

    ssl_certificate  /etc/ssl/certs/omicsBroker.crt;
    ssl_certificate_key /etc/ssl/private/omicsBroker.key;
    
    server_name XXXXXXXXXXXXXXXXXXXXXXXXXXX;

    location / {
        proxy_pass http://omicsBroker;
	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $http_host;
        proxy_redirect off;               
    }

    root   /var/www/html/example.com/;
    index index.php index.html index.htm;
	
    location /staticfiles/ {
        alias /home/app/web/staticfiles/;
    }

    location /mediafiles/ {
        alias /home/app/web/mediafiles/;
    }

}
```

**3- Update `docker-compose.prod.yml` file**

Change nginw port 

``` bash 
  [...]
  nginx:
    build: ./nginx
    volumes:
      - static_volume:/home/app/web/staticfiles
      - media_volume:/home/app/web/mediafiles
    ports:
      - 443:443 
    depends_on:
      - web
  [...]
```

## Database

The database used in this project is [postgreSQL](https://www.postgresql.org/).

### Connection

*If docker-compose is running*

```bach
docker exec -f docker-compose.prod.yml -it omicsbroker_db_1 psql -U postgres -W postgres
```

### UML schemas

To generate UML schema in png and store in `static-apps/img/` (the image is thus usable in the application).
We use [](https://github.com/django-extensions/django-extensions), [pydotplus](https://pypi.org/project/pydotplus/) 
and [graphviz](https://pypi.org/project/graphviz/)

Note : *Docker-compose must be running*

``` bash 
docker-compose -f docker-compose.prod.yml  exec web python manage.py graph_models -a -o static-apps/img/omicsBroker_models.png
```

**Result**

![](static-apps/img/omicsBroker_models.png)

## Software development phase

### Start test instance

``` bash
# Run docker-compose
docker-compose up -d --build

# Migrate models into database and
docker-compose exec web python manage.py migrate 

# Create super user
docker-compose exec web python manage.py createsuperuser

# Populate 
docker-compose exec web python manage.py load_countries
```

omicsBroker is running. You can open a web browser and use it in the following url : [http://localhost:8000/](http://localhost:8000/).

### Create a new app

``` bash
mkdir apps/new-app
docker-compose exec web python manage.py startapp new-app apps/new-app
```

Add in `settings.py` file in `INSTALLED_APPS`.

### Make migrations

``` bash
docker-compose exec web python manage.py makemigrations
docker-compose exec web python manage.py migrate
```

### Reminder

List of useful command lines in test instance:
- Close omicsBroker : `docker-compose down`
- Restart omicsBroker : `docker-compose up -d`
- Rebuild omicsBroker: `docker-compose up -d --build`
- Get logs : `docker-compose up -d logs -f`
- Populate with dummy data : `bash populate_exemple_test.sh `
- Generate UML schema : `docker-compose exec web python manage.py graph_models -a -o static-apps/img/omicsBroker_models.png`

### Code quality

Adding code to GitLab is subject to a series of code quality checks through 
continuous integration. We recommend that you run these checks before submission. 
Please note that the coverage rate is set at 100% and no errors at Flake8. 

#### Coverage
*If docker-compose is running in test mode (`docker-compose up -d --build)*

``` bash
docker-compose exec web coverage erase 
docker-compose exec web coverage run --omit='manage.py' manage.py test apps/
docker-compose exec web coverage html 
docker-compose exec web coverage-badge -o coverage.svg
```

#### Flake8
*If docker-compose is running*

**Note** : In order not to fail at the time of continuous integration, no errors should be detected.

Documentation : [Flake8](https://flake8.pycqa.org/en/latest/index.html)
``` bash
docker-compose exec web flake8 --exclude='venv/,*migrations*'
```

## Ressources & Documentations

**Docker environement & Django**

- [https://docs.docker.com/compose/django/](https://docs.docker.com/compose/django/)
- [testdriven.io](https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/#project-setup)

**Django**

- https://www.djangoproject.com/

**ENA**

- Genomic Standards Consortium : MIxS Checklist v5.0 and GitHub
- ENA submission : ENA: Guidelines and Tutorials

**Database**
- Countries : https://datahub.io/core/geo-countries

## Citation
If you use omicsBroker project, please cite us :

IFB-ElixirFr , omicsBroker, (2021), GitLab repository, https://gitlab.com/ifb-elixirfr/fair/omicsbroker

## Contributors
- Thomas Denecker (ORCID 0000-0003-1421-7641)
- Hélène Chiapello (ORCID 0000-0001-5102-0632)
- Jacques van Helden (ORCID 0000-0002-8799-8584)
- François Gerbes

## Contributing
Please, see the [CONTRIBUTING](CONTRIBUTING.md) file.

## Contributor Code of Conduct
Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/). By participating in this project you agree to abide by its terms. See [CODE_OF_CONDUCT](code_of_conduct.md) file.

## License
omicsBroker is released under the  GNU General Public License v3.0. See the bundled [LICENSE](LICENSE) file for details.