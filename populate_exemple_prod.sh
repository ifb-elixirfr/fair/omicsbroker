#!/bin/sh

docker-compose -f docker-compose.prod.yml exec web python manage.py load_countries
docker-compose -f docker-compose.prod.yml exec web python manage.py load_app_users
docker-compose -f docker-compose.prod.yml exec web python manage.py load_projects
docker-compose -f docker-compose.prod.yml exec web python manage.py load_work_on
docker-compose -f docker-compose.prod.yml exec web python manage.py import_faq